-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 14, 2015 at 12:15 AM
-- Server version: 5.5.43-0+deb8u1
-- PHP Version: 5.6.7-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fardad_abfa`
--

-- --------------------------------------------------------

--
-- Table structure for table `External_News`
--

CREATE TABLE IF NOT EXISTS `External_News` (
`id` int(11) NOT NULL,
  `source_id` int(11) DEFAULT NULL,
  `headline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lead` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `crawled` tinyint(1) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Keyword`
--

CREATE TABLE IF NOT EXISTS `Keyword` (
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aliases` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `News_Tags`
--

CREATE TABLE IF NOT EXISTS `News_Tags` (
  `news_id` int(11) NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Source`
--

CREATE TABLE IF NOT EXISTS `Source` (
`id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `homepage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `headlinePattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leadPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bodyPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photoPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastVisitedDate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `SourceLink`
--

CREATE TABLE IF NOT EXISTS `SourceLink` (
`id` int(11) NOT NULL,
  `source_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastVisitedDate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SourceLink`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `External_News`
--
ALTER TABLE `External_News`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_9B801DB736AC99F1` (`link`), ADD KEY `IDX_9B801DB7953C1C61` (`source_id`);

--
-- Indexes for table `Keyword`
--
ALTER TABLE `Keyword`
 ADD PRIMARY KEY (`tag`);

--
-- Indexes for table `News_Tags`
--
ALTER TABLE `News_Tags`
 ADD PRIMARY KEY (`news_id`,`tag`), ADD KEY `IDX_98A24F30B5A459A0` (`news_id`), ADD KEY `IDX_98A24F30389B783` (`tag`);

--
-- Indexes for table `Source`
--
ALTER TABLE `Source`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `SourceLink`
--
ALTER TABLE `SourceLink`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_C55C29A4953C1C61` (`source_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `External_News`
--
ALTER TABLE `External_News`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Source`
--
ALTER TABLE `Source`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `SourceLink`
--
ALTER TABLE `SourceLink`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `SourceLink`
--
ALTER TABLE `SourceLink`
ADD CONSTRAINT `FK_C55C29A4953C1C61` FOREIGN KEY (`source_id`) REFERENCES `Source` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
