-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 18, 2015 at 08:40 PM
-- Server version: 5.5.43-0+deb8u1
-- PHP Version: 5.6.7-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fardad_abfa`
--

-- --------------------------------------------------------

--
-- Table structure for table `External_News`
--

CREATE TABLE IF NOT EXISTS `External_News` (
`id` int(11) NOT NULL,
  `source_id` int(11) DEFAULT NULL,
  `headline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lead` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `crawled` tinyint(1) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Keyword`
--

CREATE TABLE IF NOT EXISTS `Keyword` (
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aliases` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `News_Tags`
--

CREATE TABLE IF NOT EXISTS `News_Tags` (
  `news_id` int(11) NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Source`
--

CREATE TABLE IF NOT EXISTS `Source` (
`id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `homepage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titlePattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `headlinePattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leadPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bodyPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photoPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastVisitedDate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Source`
--

INSERT INTO `Source` (`id`, `title`, `homepage`, `titlePattern`, `headlinePattern`, `leadPattern`, `bodyPattern`, `photoPattern`, `lastVisitedDate`) VALUES
(1, 'خبرگزاری فارس', 'http://www.farsnews.com/', '#nwstxtInfoPane > div.nwstxtinfotitle', '#nwstxtInfoPane > div.nwstxtrotitr', '#nwstxtInfoPane > div.nwstxtinfotitle', '#nwstxtBodyPane', '#nwstxtPicPane > img', '0000-00-00 00:00:00'),
(2, 'خبرآنلاین', 'http://www.khabaronline.ir', 'body > div > div.detailPageCont > div.rightCol > div.newsBodyCont > h1', '', 'body > div > div.detailPageCont > div.rightCol > div.newsBodyCont > div.leadCont', 'body > div > div.detailPageCont > div.rightCol > div.newsBodyCont > p', 'body > div > div.detailPageCont > div.rightCol > div.newsBodyCont > div.newsPhoto > img', '2015-05-18 00:00:00'),
(3, 'تابناک', 'http://www.tabnak.ir/', '#news > div.page_inner > div.inner_content_news > div:nth-child(2) > div.title > h1', '#news > div.page_inner > div.inner_content_news > div:nth-child(2) > div.rutitr', '#news > div.page_inner > div.inner_content_news > div:nth-child(2) > div.subtitle', '#news > div.page_inner > div.inner_content_news > div:nth-child(2) > div:nth-child(4) > div.body', '', '2015-05-18 00:00:00'),
(4, 'ایسنا', 'http://www.isna.ir', 'body > div > table > tbody > tr > td.colNews > div.news > div.titrpart > div.titr > h1', 'body > div > table > tbody > tr > td.colNews > div.news > div.titrpart > div.rutitr > h2', '', 'body > div > table > tbody > tr > td.colNews > div.news > div.body', 'body > div > table > tbody > tr > td.colNews > div.news > div.main-image > a > img', '2015-05-18 00:00:00'),
(5, 'عصرایران', 'http://www.asriran.com', '#news > div.page_news > div.col2_news > div:nth-child(2) > div:nth-child(2) > div.title > h1', '#news > div.page_news > div.col2_news > div:nth-child(2) > div:nth-child(2) > div.rutitr', '#news > div.page_news > div.col2_news > div:nth-child(2) > div:nth-child(2) > div.subtitle', '#news > div.page_news > div.col2_news > div:nth-child(2) > div:nth-child(2) > div.body', '#news > div.page_news > div.col2_news > div:nth-child(2) > div:nth-child(2) > div.body > div:nth-child(1) > img', '2015-05-18 00:00:00'),
(6, 'خبرگزاری تسنیم', 'http://www.tasnimnews.com/', '#k2Container > header > h1', '', '#k2Container > header > div > h2', '#k2Container > div.itemBody.StoryBody', '#k2Container > img', '2015-05-18 00:00:00'),
(7, 'مشرق‌نیوز', 'http://www.mashreghnews.ir/', '#news > div.container > div > div.col-xs-12.col-ms-12.col-sm-8.col-md-6 > div.news_body > div:nth-child(2) > div.title > h1 > a', '#news > div.container > div > div.col-xs-12.col-ms-12.col-sm-8.col-md-6 > div.news_body > div:nth-child(2) > div.rutitr', '#news > div.container > div > div.col-xs-12.col-ms-12.col-sm-8.col-md-6 > div.news_body > div:nth-child(2) > div.subtitle', '#news > div.container > div > div.col-xs-12.col-ms-12.col-sm-8.col-md-6 > div.news_body > div:nth-child(2) > div.body', '', '2015-05-18 00:00:00'),
(8, 'بولتن‌نیوز', 'http://www.bultannews.com/', '#news > div > div.main_body_in > div.row_3_in > div.row_3_col2_in > div:nth-child(2) > div.title > h1 > a', '#news > div > div.main_body_in > div.row_3_in > div.row_3_col2_in > div:nth-child(2) > div.rutitr', '#news > div > div.main_body_in > div.row_3_in > div.row_3_col2_in > div:nth-child(2) > div.subtitle', '#news > div > div.main_body_in > div.row_3_in > div.row_3_col2_in > div:nth-child(2) > div.body', '#news > div > div.main_body_in > div.row_3_in > div.row_3_col2_in > div:nth-child(2) > div.body > img', '2015-05-18 00:00:00'),
(10, 'فرارو', 'http://fararu.com/', '#news > div > div.news_body > div.news_col2 > div:nth-child(2) > div.title > a', '#news > div > div.news_body > div.news_col2 > div:nth-child(2) > div.rutitr', '', '#news > div > div.news_body > div.news_col2 > div:nth-child(2) > div.body', '', '0000-00-00 00:00:00'),
(11, 'ایرنا', 'http://www.irna.ir/', '#RegionContent > div.Box1 > div.RCContainer > div > div.Float > div.ContentStyle > h1', '#RegionContent > div.Box1 > div.RCContainer > div > div.Float > div.ContentStyle > h4', '#ctl00_ctl00_ContentPlaceHolder_ContentPlaceHolder_NewsContent3_H1', '#ctl00_ctl00_ContentPlaceHolder_ContentPlaceHolder_NewsContent3_bodytext', '#ctl00_ctl00_ContentPlaceHolder_ContentPlaceHolder_NewsContent3_Image1', '2015-05-18 00:00:00'),
(12, 'باشگاه خبرنگاران ', 'http://www.yjc.ir/', '#news > div.page_news > div > div.col2_news > div > div:nth-child(2) > div.title > h1 > a', '#news > div.page_news > div > div.col2_news > div > div:nth-child(2) > div.rutitr', '#news > div.page_news > div > div.col2_news > div > div:nth-child(2) > div.subtitle', '#news > div.page_news > div > div.col2_news > div > div:nth-child(2) > div.body', '', '2015-05-18 00:00:00'),
(13, 'خبرگزاری مهر', 'http://www.mehrnews.com/', '#item > article > div > div.item-title > h2', '#item > article > div > div.item-title > div', '#item > article > div > div.item-body > span', '#item > article > div > div.item-body > div', '#item > article > div > div.item-img > div > span > a > img', '2015-05-18 00:00:00'),
(14, 'ایلنا', 'http://www.ilna.ir/', 'body > section > div.xcon12.main.mt16.mb32 > div > div.column_one.float > div > article > header > h1 > a', 'body > section > div.xcon12.main.mt16.mb32 > div > div.column_one.float > div > article > header > h2', 'body > section > div.xcon12.main.mt16.mb32 > div > div.column_one.float > div > article > section.article_body.mt16.clearbox.fn14.content > p.fn14.news_lead.pr8.pl8.pt8.pb8', 'body > section > div.xcon12.main.mt16.mb32 > div > div.column_one.float > div > article > section.article_body.mt16.clearbox.fn14.content > p', 'body > section > div.xcon12.main.mt16.mb32 > div > div.column_one.float > div > article > section.article_body.mt16.clearbox.fn14.content > div > a > img', '2015-05-18 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `SourceLink`
--

CREATE TABLE IF NOT EXISTS `SourceLink` (
`id` int(11) NOT NULL,
  `source_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastVisitedDate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SourceLink`
--

INSERT INTO `SourceLink` (`id`, `source_id`, `url`, `lastVisitedDate`) VALUES
(1, 1, 'http://www.farsnews.com/rss.php', '2015-05-13 23:31:25'),
(2, 1, 'http://farsnews.com/rss.php?srv=1', '0000-00-00 00:00:00'),
(3, 1, 'http://farsnews.com/rss.php?srv=2', '2015-05-18 00:00:00'),
(4, 1, 'http://farsnews.com/rss.php?srv=3', '2015-05-18 00:00:00'),
(5, 1, 'http://farsnews.com/rss.php?srv=4', '2015-05-18 00:00:00'),
(6, 1, 'http://farsnews.com/rss.php?srv=6', '2015-05-18 00:00:00'),
(7, 1, 'http://farsnews.com/rss.php?srv=7', '2015-05-18 00:00:00'),
(8, 1, 'http://farsnews.com/rss.php?srv=8', '2015-05-18 00:00:00'),
(9, 1, 'http://farsnews.com/rss.php?srv=11', '2015-05-18 00:00:00'),
(10, 2, 'http://khabaronline.ir/RSS', '2015-05-18 00:00:00'),
(11, 2, 'http://khabaronline.ir/RSS/Service/politics', '2015-05-18 00:00:00'),
(12, 2, 'http://khabaronline.ir/RSS/Service/economy', '2015-05-18 00:00:00'),
(13, 2, 'http://khabaronline.ir/RSS/Service/culture', '2015-05-18 00:00:00'),
(14, 2, 'http://khabaronline.ir/RSS/Service/society', '2015-05-18 00:00:00'),
(15, 2, 'http://khabaronline.ir/RSS/Service/World', '2015-05-18 00:00:00'),
(16, 2, 'http://khabaronline.ir/RSS/Service/sport', '2015-05-18 00:00:00'),
(17, 2, 'http://khabaronline.ir/RSS/Service/science', '2015-05-18 00:00:00'),
(18, 2, 'http://khabaronline.ir/RSS/Service/ict', '2015-05-18 00:00:00'),
(19, 3, 'http://www.tabnak.ir/fa/rss/allnews', '0000-00-00 00:00:00'),
(20, 3, 'http://www.tabnak.ir/fa/rss/1', '0000-00-00 00:00:00'),
(21, 3, 'http://www.tabnak.ir/fa/rss/2', '0000-00-00 00:00:00'),
(22, 3, 'http://www.tabnak.ir/fa/rss/3', '0000-00-00 00:00:00'),
(23, 3, 'http://www.tabnak.ir/fa/rss/5', '0000-00-00 00:00:00'),
(24, 3, 'http://www.tabnak.ir/fa/rss/6', '0000-00-00 00:00:00'),
(25, 3, 'http://www.tabnak.ir/fa/rss/11', '0000-00-00 00:00:00'),
(26, 4, 'http://www.isna.ir/fa/all/feed', '0000-00-00 00:00:00'),
(27, 4, 'http://www.isna.ir/fa/mainPage/feed', '0000-00-00 00:00:00'),
(28, 4, 'http://www.isna.ir/fa/Politics/feed', '0000-00-00 00:00:00'),
(29, 4, 'http://www.isna.ir/fa/Science%20and%20Tech/feed', '0000-00-00 00:00:00'),
(30, 4, 'http://www.isna.ir/fa/World/feed', '0000-00-00 00:00:00'),
(31, 4, 'http://www.isna.ir/fa/Culture%20and%20Art/feed', '0000-00-00 00:00:00'),
(32, 4, 'http://www.isna.ir/fa/Sports/feed', '0000-00-00 00:00:00'),
(33, 4, 'http://www.isna.ir/fa/province/feed', '0000-00-00 00:00:00'),
(34, 4, 'http://www.isna.ir/fa/Market/feed', '0000-00-00 00:00:00'),
(35, 5, 'http://www.asriran.com/fa/rss/allnews', '0000-00-00 00:00:00'),
(36, 5, 'http://www.asriran.com/fa/rss/1', '0000-00-00 00:00:00'),
(37, 5, 'http://www.asriran.com/fa/rss/1/1', '0000-00-00 00:00:00'),
(38, 5, 'http://www.asriran.com/fa/rss/1/2', '0000-00-00 00:00:00'),
(39, 5, 'http://www.asriran.com/fa/rss/1/3', '0000-00-00 00:00:00'),
(40, 5, 'http://www.asriran.com/fa/rss/1/4', '0000-00-00 00:00:00'),
(41, 5, 'http://www.asriran.com/fa/rss/1/5', '0000-00-00 00:00:00'),
(42, 4, 'http://www.asriran.com/fa/rss/1/6', '0000-00-00 00:00:00'),
(43, 5, 'http://www.asriran.com/fa/rss/1/7', '0000-00-00 00:00:00'),
(44, 5, 'http://www.asriran.com/fa/rss/1/8', '0000-00-00 00:00:00'),
(45, 5, 'http://www.asriran.com/fa/rss/1/9', '0000-00-00 00:00:00'),
(46, 5, 'http://www.asriran.com/fa/rss/1/10', '0000-00-00 00:00:00'),
(47, 5, 'http://www.asriran.com/fa/rss/1/14', '0000-00-00 00:00:00'),
(48, 6, 'http://www.tasnimnews.com/rss/feed/?c=1&m=6&alt=%D8%A2%D8%AE%D8%B1%DB%8C%D9%86%20%D8%A7%D8%AE%D8%A8%D8%A7%D8%B1', '0000-00-00 00:00:00'),
(49, 6, 'http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=1', '0000-00-00 00:00:00'),
(50, 6, 'http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=8', '0000-00-00 00:00:00'),
(51, 6, 'http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=1115', '0000-00-00 00:00:00'),
(52, 6, 'http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=4', '0000-00-00 00:00:00'),
(53, 6, 'http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=7', '0000-00-00 00:00:00'),
(54, 6, 'http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=2', '0000-00-00 00:00:00'),
(55, 6, 'http://www.tasnimnews.com/rss/feed/?c=1&m=6&cat=3', '0000-00-00 00:00:00'),
(56, 7, 'http://www.mashreghnews.ir/fa/rss/allnews', '0000-00-00 00:00:00'),
(57, 7, 'http://www.mashreghnews.ir/fa/rss/1', '0000-00-00 00:00:00'),
(58, 7, 'http://www.mashreghnews.ir/fa/rss/2', '0000-00-00 00:00:00'),
(59, 7, 'http://www.mashreghnews.ir/fa/rss/4', '0000-00-00 00:00:00'),
(60, 7, 'http://www.mashreghnews.ir/fa/rss/16', '0000-00-00 00:00:00'),
(61, 7, 'http://www.mashreghnews.ir/fa/rss/5', '0000-00-00 00:00:00'),
(62, 7, 'http://www.mashreghnews.ir/fa/rss/7', '0000-00-00 00:00:00'),
(63, 7, 'http://www.mashreghnews.ir/fa/rss/10', '0000-00-00 00:00:00'),
(64, 7, 'http://www.mashreghnews.ir/fa/rss/11', '0000-00-00 00:00:00'),
(65, 7, 'http://www.mashreghnews.ir/fa/rss/14', '0000-00-00 00:00:00'),
(66, 8, 'http://www.bultannews.com/fa/rss/allnews', '0000-00-00 00:00:00'),
(67, 8, 'http://www.bultannews.com/fa/rss/1', '0000-00-00 00:00:00'),
(68, 8, 'http://www.bultannews.com/fa/rss/2', '0000-00-00 00:00:00'),
(69, 8, 'http://www.bultannews.com/fa/rss/4', '0000-00-00 00:00:00'),
(70, 8, 'http://www.bultannews.com/fa/rss/5', '0000-00-00 00:00:00'),
(71, 8, 'http://www.bultannews.com/fa/rss/8', '0000-00-00 00:00:00'),
(72, 10, 'http://fararu.com/fa/rss/allnews', '0000-00-00 00:00:00'),
(73, 10, 'http://fararu.com/fa/rss/1', '0000-00-00 00:00:00'),
(74, 10, 'http://fararu.com/fa/rss/2', '0000-00-00 00:00:00'),
(75, 10, 'http://fararu.com/fa/rss/3', '0000-00-00 00:00:00'),
(76, 10, 'http://fararu.com/fa/rss/10', '0000-00-00 00:00:00'),
(77, 10, 'http://fararu.com/fa/rss/4', '0000-00-00 00:00:00'),
(78, 11, 'http://www.irna.ir/fa/rss.aspx?kind=-1', '0000-00-00 00:00:00'),
(79, 11, 'http://www.irna.ir/fa/rss.aspx?kind=5', '0000-00-00 00:00:00'),
(80, 11, 'http://www.irna.ir/fa/rss.aspx?kind=20', '0000-00-00 00:00:00'),
(81, 11, 'http://www.irna.ir/fa/rss.aspx?kind=32', '0000-00-00 00:00:00'),
(82, 11, 'http://www.irna.ir/fa/rss.aspx?kind=41', '0000-00-00 00:00:00'),
(83, 11, 'http://www.irna.ir/fa/rss.aspx?kind=180', '0000-00-00 00:00:00'),
(84, 11, 'http://www.irna.ir/fa/rss.aspx?kind=14', '0000-00-00 00:00:00'),
(85, 11, 'http://www.irna.ir/fa/rss.aspx?kind=1', '0000-00-00 00:00:00'),
(86, 11, 'http://www.irna.ir/fa/rss.aspx?kind=54', '0000-00-00 00:00:00'),
(87, 12, 'http://www.yjc.ir/fa/rss/allnews', '0000-00-00 00:00:00'),
(88, 12, 'http://www.yjc.ir/fa/rss/1', '0000-00-00 00:00:00'),
(89, 12, 'http://www.yjc.ir/fa/rss/3', '0000-00-00 00:00:00'),
(90, 12, 'http://www.yjc.ir/fa/rss/4', '0000-00-00 00:00:00'),
(91, 12, 'http://www.yjc.ir/fa/rss/5', '0000-00-00 00:00:00'),
(92, 12, 'http://www.yjc.ir/fa/rss/6', '0000-00-00 00:00:00'),
(93, 12, 'http://www.yjc.ir/fa/rss/7', '0000-00-00 00:00:00'),
(94, 12, 'http://www.yjc.ir/fa/rss/8', '0000-00-00 00:00:00'),
(95, 12, 'http://www.yjc.ir/fa/rss/9', '0000-00-00 00:00:00'),
(96, 12, 'http://www.yjc.ir/fa/rss/14', '0000-00-00 00:00:00'),
(97, 13, 'http://www.mehrnews.com/rss', '0000-00-00 00:00:00'),
(98, 13, 'http://www.mehrnews.com/rss-homepage', '0000-00-00 00:00:00'),
(99, 13, 'http://www.mehrnews.com/rss?tp=2', '0000-00-00 00:00:00'),
(100, 14, 'http://www.ilna.ir/feeds/', '0000-00-00 00:00:00'),
(101, 14, 'http://www.ilna.ir/fa/feeds/?p=Y2F0ZWdvcmllcz0z', '0000-00-00 00:00:00'),
(102, 14, 'http://www.ilna.ir/fa/feeds/?p=Y2F0ZWdvcmllcz00', '0000-00-00 00:00:00'),
(103, 14, 'http://www.ilna.ir/fa/feeds/?p=Y2F0ZWdvcmllcz05', '0000-00-00 00:00:00'),
(104, 14, 'http://www.ilna.ir/fa/feeds/?p=Y2F0ZWdvcmllcz01', '0000-00-00 00:00:00'),
(105, 14, 'http://www.ilna.ir/fa/feeds/?p=Y2F0ZWdvcmllcz02', '0000-00-00 00:00:00'),
(106, 14, 'http://www.ilna.ir/fa/feeds/?p=Y2F0ZWdvcmllcz04', '0000-00-00 00:00:00'),
(107, 14, 'http://www.ilna.ir/fa/feeds/?p=Y2F0ZWdvcmllcz03', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `External_News`
--
ALTER TABLE `External_News`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_9B801DB736AC99F1` (`link`), ADD KEY `IDX_9B801DB7953C1C61` (`source_id`);

--
-- Indexes for table `Keyword`
--
ALTER TABLE `Keyword`
 ADD PRIMARY KEY (`tag`);

--
-- Indexes for table `News_Tags`
--
ALTER TABLE `News_Tags`
 ADD PRIMARY KEY (`news_id`,`tag`), ADD KEY `IDX_98A24F30B5A459A0` (`news_id`), ADD KEY `IDX_98A24F30389B783` (`tag`);

--
-- Indexes for table `Source`
--
ALTER TABLE `Source`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `SourceLink`
--
ALTER TABLE `SourceLink`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_C55C29A4953C1C61` (`source_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `External_News`
--
ALTER TABLE `External_News`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Source`
--
ALTER TABLE `Source`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `SourceLink`
--
ALTER TABLE `SourceLink`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `SourceLink`
--
ALTER TABLE `SourceLink`
ADD CONSTRAINT `FK_C55C29A4953C1C61` FOREIGN KEY (`source_id`) REFERENCES `Source` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
