<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsPhoto
 *
 * @ORM\Table(name="news_photos", indexes={@ORM\Index(name="news_id", columns={"news_id"})})
 * @ORM\Entity
 */
class NewsPhoto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="upload_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $uploadDate;

    /**
     * @var \Application\Entity\News
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\News")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="news_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $news;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return NewsPhoto
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set uploadDate
     *
     * @param \DateTime $uploadDate
     * @return NewsPhoto
     */
    public function setUploadDate($uploadDate)
    {
        $this->uploadDate = $uploadDate;

        return $this;
    }

    /**
     * Get uploadDate
     *
     * @return \DateTime 
     */
    public function getUploadDate()
    {
        return $this->uploadDate;
    }

    /**
     * Set news
     *
     * @param \Application\Entity\News $news
     * @return NewsPhoto
     */
    public function setNews(\Application\Entity\News $news = null)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news
     *
     * @return \Application\Entity\News 
     */
    public function getNews()
    {
        return $this->news;
    }
}
