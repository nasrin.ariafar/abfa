<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class BaseRepository extends EntityRepository
{
	public function findAllByLastUpdate($lastUpdate)
	{
		if (empty($lastUpdate)) {
			return $this->findAll();
		}

		$q = $this->createQueryBuilder('i')
			->where('i.modifiedDate > :lastUpdate')
			->setParameter('lastUpdate', $lastUpdate)
			->getQuery();

		return $q->getResult();
	}
}
