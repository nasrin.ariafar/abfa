<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_8D93D649F85E0677", columns={"username"}), @ORM\UniqueConstraint(name="UNIQ_8D93D649E7927C74", columns={"email"})})
 * @ORM\Entity(repositoryClass="Application\Entity\BaseRepository")
 */
class User implements \ZfcUser\Entity\UserInterface
{

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $displayName;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=128, precision=0, scale=0, nullable=false, unique=false)
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="accessibility", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $accessibility = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", nullable=false)
     */
    private $modifiedDate = '2012-06-23 00:00:00';

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     * @return User
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return User
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $username;

    /**
     * @var integer
     *
     * @ORM\Column(name="registration_code", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $registrationCode;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set registrationCode
     *
     * @param integer $registrationCode
     * @return User
     */
    public function setRegistrationCode($registrationCode)
    {
        $this->registrationCode = $registrationCode;

        return $this;
    }

    /**
     * Get registrationCode
     *
     * @return integer
     */
    public function getRegistrationCode()
    {
        return $this->registrationCode;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="gcm_token", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $gcmToken;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\Level", mappedBy="user")
     */
    private $level;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contactGroup = new \Doctrine\Common\Collections\ArrayCollection();
        $this->event = new \Doctrine\Common\Collections\ArrayCollection();
        $this->level = new \Doctrine\Common\Collections\ArrayCollection();
        $this->modifiedDate = new \DateTime('now');
    }

    /**
     * Set gcmToken
     *
     * @param string $gcmToken
     * @return User
     */
    public function setGcmToken($gcmToken)
    {
        $this->gcmToken = $gcmToken;

        return $this;
    }

    /**
     * Get gcmToken
     *
     * @return string
     */
    public function getGcmToken()
    {
        return $this->gcmToken;
    }

    /**
     * Add level
     *
     * @param \Application\Entity\Level $level
     * @return User
     */
    public function addLevel(\Application\Entity\Level $level)
    {
        $this->level[] = $level;

        return $this;
    }

    /**
     * Remove level
     *
     * @param \Application\Entity\Level $level
     */
    public function removeLevel(\Application\Entity\Level $level)
    {
        $this->level->removeElement($level);
    }

    /**
     * Get level
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @var \Application\Entity\Region
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * Set region
     *
     * @param \Application\Entity\Region $region
     * @return User
     */
    public function setRegion(\Application\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Application\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * Set photo
     *
     * @param string $photo
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\Event", mappedBy="user")
     */
    private $event;

    /**
     * Add event
     *
     * @param \Application\Entity\Event $event
     * @return User
     */
    public function addEvent(\Application\Entity\Event $event)
    {
        $this->event[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \Application\Entity\Event $event
     */
    public function removeEvent(\Application\Entity\Event $event)
    {
        $this->event->removeElement($event);
    }

    /**
     * Get event
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvent()
    {
        return $this->event;
    }

    public function setId($id)
    {

    }

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=64, precision=0, scale=0, nullable=true, unique=false)
     */
    private $position;

    /**
     * Get position.
     *
     * @return position.
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position.
     *
     * @param position the value to set.
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="`type`", type="string", length=128, precision=0, scale=0, nullable=false, unique=false)
     */
    private $type = 'USER';

    /**
     * Get type.
     *
     * @return type.
     */
    function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param type the value to set.
     */
    function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $lastname;

    /**
     * Get firstname.
     *
     * @return firstname.
     */
    function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set firstname.
     *
     * @param firstname the value to set.
     */
    function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Get lastname.
     *
     * @return lastname.
     */
    function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set lastname.
     *
     * @param lastname the value to set.
     */
    function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Set accessibility
     *
     * @param integer $accessibility
     * @return User
     */
    public function setAccessibility($accessibility)
    {
        $this->accessibility = $accessibility;

        return $this;
    }

    /**
     * Get accessibility
     *
     * @return integer
     */
    public function getAccessibility()
    {
        return $this->accessibility;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     *
     * @return User
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ContactGroup", inversedBy="user")
     * @ORM\JoinTable(name="contact_group_users",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="contact_group_id", referencedColumnName="id")
     *   }
     * )
     */
    private $contactGroup;

    /**
     * Add contactGroup
     *
     * @param \Application\Entity\ContactGroup $contactGroup
     *
     * @return User
     */
    public function addContactGroup(\Application\Entity\ContactGroup $contactGroup)
    {
        $this->contactGroup[] = $contactGroup;

        return $this;
    }

    /**
     * Remove contactGroup
     *
     * @param \Application\Entity\ContactGroup $contactGroup
     */
    public function removeContactGroup(\Application\Entity\ContactGroup $contactGroup)
    {
        $this->contactGroup->removeElement($contactGroup);
    }

    /**
     * Get contactGroup
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactGroup()
    {
        return $this->contactGroup;
    }

    public function isAdmin()
    {
        return strtolower($this->type) == 'admin';
    }
}
