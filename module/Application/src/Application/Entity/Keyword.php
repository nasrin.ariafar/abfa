<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Keyword
 *
 * @ORM\Table(name="Keyword")
 * @ORM\Entity(repositoryClass="Application\Entity\KeywordRepository")
 */
class Keyword
{
    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="aliases", type="string")
     */
    private $aliases;


    /**
     * Set tag
     *
     * @param string $tag
     * @return Keyword
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set aliases
     *
     * @param string $aliases
     * @return Keyword
     */
    public function setAliases($aliases)
    {
        $this->aliases = $aliases;

        return $this;
    }

    /**
     * Get aliases
     *
     * @return string
     */
    public function getAliases()
    {
        return $this->aliases;
    }
}
