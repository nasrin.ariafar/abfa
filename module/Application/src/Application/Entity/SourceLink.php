<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sourcelink
 *
 * @ORM\Table(name="SourceLink", indexes={@ORM\Index(name="source_id", columns={"source_id"})})
 * @ORM\Entity
 */
class SourceLink
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var \Application\Entity\Source
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Source")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="source_id", referencedColumnName="id")
     * })
     */
    private $source;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastVisitedDate", type="datetime", nullable=false)
     */
    private $lastvisiteddate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return SourceLink
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set source
     *
     * @param \Application\Entity\Source $source
     * @return SourceLink
     */
    public function setSource(\Application\Entity\Source $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \Application\Entity\Source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set lastvisiteddate
     *
     * @param \DateTime $lastvisiteddate
     * @return Source
     */
    public function setLastvisiteddate($lastvisiteddate)
    {
        $this->lastvisiteddate = $lastvisiteddate;

        return $this;
    }

    /**
     * Get lastvisiteddate
     *
     * @return \DateTime
     */
    public function getLastvisiteddate()
    {
        return $this->lastvisiteddate;
    }
}
