<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Source
 *
 * @ORM\Table(name="Source")
 * @ORM\Entity
 */
class Source
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="homepage", type="string", length=255, nullable=false)
     */
    private $homepage;

    /**
     * @var string
     *
     * @ORM\Column(name="headlinePattern", type="string", length=255, nullable=false)
     */
    private $headlinepattern;

    /**
     * @var string
     *
     * @ORM\Column(name="titlePattern", type="string", length=255, nullable=false)
     */
    private $titlepattern;

    /**
     * @var string
     *
     * @ORM\Column(name="leadPattern", type="string", length=255, nullable=false)
     */
    private $leadpattern;

    /**
     * @var string
     *
     * @ORM\Column(name="bodyPattern", type="string", length=255, nullable=false)
     */
    private $bodypattern;

    /**
     * @var string
     *
     * @ORM\Column(name="photoPattern", type="string", length=255, nullable=false)
     */
    private $photopattern;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastVisitedDate", type="datetime", nullable=false)
     */
    private $lastvisiteddate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Source
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set homepage
     *
     * @param string $homepage
     * @return Source
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;

        return $this;
    }

    /**
     * Get homepage
     *
     * @return string
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * Set headlinepattern
     *
     * @param string $headlinepattern
     * @return Source
     */
    public function setHeadlinepattern($headlinepattern)
    {
        $this->headlinepattern = $headlinepattern;

        return $this;
    }

    /**
     * Get headlinepattern
     *
     * @return string
     */
    public function getHeadlinepattern()
    {
        return $this->headlinepattern;
    }

    /**
     * Set titlepattern
     *
     * @param string $titlepattern
     * @return Source
     */
    public function setTitlePattern($titlepattern)
    {
        $this->titlepattern = $titlepattern;

        return $this;
    }

    /**
     * Get headlinepattern
     *
     * @return string
     */
    public function getTitlepattern()
    {
        return $this->titlepattern;
    }

    /**
     * Set leadpattern
     *
     * @param string $leadpattern
     * @return Source
     */
    public function setLeadpattern($leadpattern)
    {
        $this->leadpattern = $leadpattern;

        return $this;
    }

    /**
     * Get leadpattern
     *
     * @return string
     */
    public function getLeadpattern()
    {
        return $this->leadpattern;
    }

    /**
     * Set bodypattern
     *
     * @param string $bodypattern
     * @return Source
     */
    public function setBodypattern($bodypattern)
    {
        $this->bodypattern = $bodypattern;

        return $this;
    }

    /**
     * Get bodypattern
     *
     * @return string
     */
    public function getBodypattern()
    {
        return $this->bodypattern;
    }

    /**
     * Set photopattern
     *
     * @param string $photopattern
     * @return Source
     */
    public function setPhotopattern($photopattern)
    {
        $this->photopattern = $photopattern;

        return $this;
    }

    /**
     * Get photopattern
     *
     * @return string
     */
    public function getPhotopattern()
    {
        return $this->photopattern;
    }

    /**
     * Set lastvisiteddate
     *
     * @param \DateTime $lastvisiteddate
     * @return Source
     */
    public function setLastvisiteddate($lastvisiteddate)
    {
        $this->lastvisiteddate = $lastvisiteddate;

        return $this;
    }

    /**
     * Get lastvisiteddate
     *
     * @return \DateTime
     */
    public function getLastvisiteddate()
    {
        return $this->lastvisiteddate;
    }
}
