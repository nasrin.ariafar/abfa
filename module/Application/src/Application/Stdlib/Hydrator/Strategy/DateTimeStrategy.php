<?php
namespace Application\Stdlib\Hydrator\Strategy;

use DateTime,
    Zend\Stdlib\Hydrator\Strategy\DefaultStrategy;

class DateTimeStrategy extends DefaultStrategy
{
    private $format;

    public function __construct($format = 'Y-m-d H:i:s')
    {
        $this->format = $format;
    }

    /**
     * {@inheritdoc}
     *
     * Convert a string value into a DateTime object
     */
    public function hydrate($value)
    {
        if (is_string($value) && "" === $value) {
            $value = null;
        } elseif (is_string($value)) {
            $value = new DateTime($value);
        }

        return $value;
    }

    public function extract($value)
    {
        if ($value instanceof DateTime) {
            $value = $value->format($this->format);
        }

        return $value;
    }
}
