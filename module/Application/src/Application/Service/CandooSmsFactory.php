<?php

namespace Application\Service;

use Application\Service\CandooSms;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

class CandooSmsFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $sl)
    {
        $config  = $sl->get('Config');
        $config  = isset($config['sms']) ? $config['sms'] : array();

        $service = new CandooSms($config);
        $service->setServiceLocator($sl);

        return $service;
    }
}
