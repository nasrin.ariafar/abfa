<?php

namespace Application\Service;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

class HydratorFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $sl)
    {
        $hydrator = new DoctrineObject($sl->get('Doctrine\ORM\EntityManager'));
        $hydrator->addStrategy('datetime', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('creationDate', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('modifiedDate', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('date', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('reportTime', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('happeningTime', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('updateTime', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('updateDate', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('uploadDate', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());

        return $hydrator;
    }
}
