<?php
namespace Application\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZendService\Google\Gcm\Client;
use ZendService\Google\Gcm\Message;
use ZendService\Google\Exception\RuntimeException;

class GcmService implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;

    protected $client;

    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function __construct()
    {
        $key = "AIzaSyAPbJ2Szga3CCCKwbY6p0ZTpu8p9oJeXvg";///TODO

        $this->client = new Client();
        $this->client->getHttpClient()->setOptions(array('sslcapath' => '/etc/ssl/certs', 'sslverifypeer' => false));
        $this->client->setApiKey($key);
    }

    public function send($data, $ids)
    {
        $message = new Message();

        $message->setRegistrationIds($ids);
        // optional fields
        $message->setData($data);

        $message->setCollapseKey('event');
        $message->setRestrictedPackageName('com.afaraan.ema.android.app');
        $message->setDelayWhileIdle(false);
        $message->setTimeToLive(600);
        $message->setDryRun(false);

        try {
            $response = $this->client->send($message);
        } catch (\Exception $e) {
        }
    }
}
