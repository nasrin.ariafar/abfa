<?php

namespace Application\Service;

use Application\Service\SendSms;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

class SendSmsFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $sl)
    {
        $config  = $sl->get('Config');
        $config  = isset($config['sms']) ? $config['sms'] : array();

        $service = new SendSms($config);
        $service->setServiceLocator($sl);

        return $service;
    }
}
