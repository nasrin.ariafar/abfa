<?php

namespace Application\Service;

use Application\Service\GcmService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

class GcmServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $sl)
    {
        $service = new GcmService();
        $service->setServiceLocator($sl);

        return $service;
    }
}
