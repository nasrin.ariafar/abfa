<?php
namespace Application\Controller;

use Application\Entity\Event;
use Application\Entity\EventCoordination;
use Application\Entity\EventResource;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class WebServiceController extends AbstractRestfulController
{
    public function createAction()
    {
        $data = $this->getRequest()->getContent();
        $data = json_decode($data, true);
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy =  $this->getServiceLocator()->get('Hydrator');

        //TODO check IP
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        /*
        if (!in_array($ip, array(''))) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('status' => 'invalid ip'));
        }
         */

        $builder = new AnnotationBuilder();
        $entity  = new Event();
        $form    = $builder->createForm($entity);

        $form->setHydrator($hy);
        $form->bind($entity);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();

        $region = $entity->getRegion();
        $lastFl = 'data/ids/' . $region->getCode();
        $lastId = is_file($lastFl) ? file_get_contents($lastFl) : 100;

        $lastId++;

        file_put_contents($lastFl, $lastId);

        $id = $region->getCode() . '-' . $lastId;

        $entity->setId($id);
        $entity->setReportTime(new \DateTime('now'));
        $entity->setUpdateTime(new \DateTime('now'));
        $entity->setReadonly(true);
        $entity->setSource('WEBSERVICE');
        $entity->setCreator(null);

        $em->persist($entity);

        $data['causes'] = isset($data['causes']) ? $data['causes'] : array();
        foreach ($data['causes'] as $cid) {
            $res = $em->getRepository('Application\Entity\Cause')->find($cid);
            if ($res) {
                $entity->addCause($res);
            }
        }

        $data['types'] = isset($data['types']) ? $data['types'] : array();
        foreach ($data['types'] as $cid) {
            $res = $em->getRepository('Application\Entity\Type')->find($cid);
            if ($res) {
                $entity->addType($res);
            }
        }

        $data['users'] = isset($data['users']) ? $data['users'] : array();
        foreach ($data['users'] as $uid) {
            $ouser = $em->getRepository('Application\Entity\User')->findOneByUsername($uid);

            if ($ouser) {
                $entity->removeUser($ouser);
                $entity->addUser($ouser);
            }
        }

        $em->flush();

        $data = $hy->extract($entity);
        $data['user'] = $this->extract($this->identity());
        $data['level'] = $hy->extract($data['level']);
        $data['region'] = $hy->extract($data['region']);
        $data['levels'] = array();
        $data['types'] = array();

        foreach ($entity->getCause()->toArray() as $row) {
            $data['causes'][] = $hy->extract($row);
        }

        foreach ($entity->getType()->toArray() as $row) {
            $data['types'][] = $hy->extract($row);
        }

        $this->notifyUsers($entity, $data, $entity->getCreator(), true);

        return new JsonModel($data);
    }
}
