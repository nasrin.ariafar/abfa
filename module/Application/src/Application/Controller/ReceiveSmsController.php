<?php
namespace Application\Controller;

use Application\Entity\Event;
use Application\Entity\EventCoordination;
use Application\Entity\EventResource;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class ReceiveSmsController extends AbstractRestfulController
{
    public function receiveAction()
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $req = $this->getRequest();
        $from = $req->getQuery('from', '');
        $text = $req->getQuery('text', '');

        if (empty($text) || empty($from)) {
            return new JsonModel(array('error' => 'Text OR From is empty'));
        }

        $from = '0' . substr($from, -10);
        $user = $em->getRepository('Application\Entity\User')
            ->findOneBy(array('username' => $from));

        if (!$user) {
            return new JsonModel(array('error' => 'invalid user'));
        }

        $data = json_decode($text, true);

        if (!$data or !is_array($data)) {
            return new JsonModel(array('error' => 'invalid text'));
        }

        if (isset($data['event'])) {
            return $this->registerEvent($data['event'], $user);
        } elseif (isset($data['comment'])) {
            return $this->registerComment($data['comment'], $user);
        }

        return new JsonModel(array('error' => 'invalid content'));
    }

    protected function registerEvent($data, $user)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $builder = new AnnotationBuilder();
        $entity  = new Event();
        $form    = $builder->createForm($entity);

        $form->setHydrator($hy);
        $form->bind($entity);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();

        $region = $entity->getRegion();
        $lastFl = 'data/ids/' . $region->getCode();
        $lastId = is_file($lastFl) ? file_get_contents($lastFl) : 100;

        $lastId++;

        file_put_contents($lastFl, $lastId);

        $id = $region->getCode() . '-' . $lastId;

        $entity->setId($id);
        $entity->setReportTime(new \DateTime('now'));
        $entity->setCreator($this->identity());

        $em->persist($entity);

        $data['causes'] = isset($data['causes']) ? $data['causes'] : array();
        foreach ($data['causes'] as $cid) {
            $res = $em->getRepository('Application\Entity\Cause')->find($cid);
            if ($res) {
                $entity->addCause($res);
            }
        }

        $data['types'] = isset($data['types']) ? $data['types'] : array();
        foreach ($data['types'] as $cid) {
            $res = $em->getRepository('Application\Entity\Type')->find($cid);
            if ($res) {
                $entity->addType($res);
            }
        }

        $data['coordinations'] = isset($data['coordinations']) ? $data['coordinations'] : array();
        foreach ($data['coordinations'] as $row) {
            $resId = $row['id'];
            $desc  = isset($row['description']) ? $row['description'] : null;

            $res = $em->getRepository('Application\Entity\Coordination')->find($resId);

            if ($res) {
                $evres = new EventCoordination();
                $evres->setEvent($entity);
                $evres->setCoordination($res);
                $evres->setDescription($desc);
                $em->persist($evres);
            }
        }

        $data['resources'] = isset($data['resources']) ? $data['resources'] : array();
        foreach ($data['resources'] as $row) {
            $resId = $row['id'];
            $desc  = isset($row['description']) ? $row['description'] : null;
            $count = isset($row['count']) ? $row['count'] : 1;

            $res = $em->getRepository('Application\Entity\Resource')->find($resId);
            if ($res) {
                $evres = new EventResource();
                $evres->setEvent($entity);
                $evres->setResource($res);
                $evres->setDescription($desc);
                $evres->setCount($count);
                $em->persist($evres);
            }
        }

        $data['users'] = isset($data['users']) ? $data['users'] : array();
        foreach ($data['users'] as $uid) {
            $ouser = $em->getRepository('Application\Entity\User')->find($uid);

            if ($ouser) {
                $entity->removeUser($ouser);
                $entity->addUser($ouser);
            }
        }

        $em->flush();

        $data = $hy->extract($entity);
        $data['user'] = $this->extract($this->identity());
        $data['level'] = $hy->extract($data['level']);
        $data['region'] = $hy->extract($data['region']);
        $data['levels'] = array();
        $data['types'] = array();

        foreach ($entity->getCause()->toArray() as $row) {
            $data['causes'][] = $hy->extract($row);
        }

        foreach ($entity->getType()->toArray() as $row) {
            $data['types'][] = $hy->extract($row);
        }

        $this->notifyUsers($entity, $data, $user);

        return new JsonModel($data);
    }

    protected function registerComment($data, $user)
    {
        $sm  = $this->getServiceLocator();
        $em  = $sm->get('Doctrine\ORM\EntityManager');
        $hy  = $sm->get('Hydrator');

        $evId  = $data['event_id'];
        $event = $em->getRepository('Application\Entity\Event')->find($evId);

        if (!$event instanceof Event) {
            return new JsonModel(array('error' => 'Event not found'));
        }

        $builder = new AnnotationBuilder();
        $entity  = new \Application\Entity\EventComment();
        $form    = $builder->createForm($entity);

        $form->setHydrator($hy);
        $form->bind($entity);
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $event->setUpdateTime(new \DateTime('now'));

        $comment = $form->getData();
        $comment->setEvent($event);
        $comment->setUser($user);
        $comment->setCreationDate(new \DateTime('now'));

        $em->persist($comment);
        $em->flush();

        return new JsonModel($this->extract($comment));
    }
}
