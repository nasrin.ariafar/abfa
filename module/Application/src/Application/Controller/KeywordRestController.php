<?php
namespace Application\Controller;

use Application\Entity\Keyword;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class KeywordRestController extends AbstractRestfulController
{
    public function getList()
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy =  $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $user = $this->identity();
        $return = array();

        $qb  = $em->getRepository('Application\Entity\Keyword')->createQueryBuilder('k');
        $and = $qb->expr()->andX();

        $q = $this->getRequest()->getQuery('q', '');

        if (!empty($q)) {
            $and->add($qb->expr()->eq('k.tag', '?1'));
            $qb->setParameter(1, '%' . $q . '%');
        }

        if ($and->count() > 0) {
            $qb->where($and);
        }

        $query = $qb->getQuery();
        foreach ($query->getResult() as $row) {
            $k = array(
                'tag' => $row->getTag(),
                'aliases' => (array) json_decode($row->getAliases())
            );

            $return[] = $k;
        }

        return new JsonModel($return);
    }

    public function get($id)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy =  $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            //$this->getResponse()->setStatusCode(401);
            //return new JsonModel(array('error' => 'Unauthorized'));
        }

        $keyword = $em->find('Application\Entity\Keyword', $id);

        if (!$keyword) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $data = array(
            'tag'     => $keyword->getTag(),
            'aliases' => (array) json_decode($keyword->getAliases())
        );

        return new JsonModel($data);
    }

    public function create($data)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy =  $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            //$this->getResponse()->setStatusCode(401);
            //return new JsonModel(array('error' => 'Unauthorized'));
        }

        $keyword = $em->getRepository('Application\Entity\Keyword')->getByTag($data['tag']);
        $keyword->setAliases(json_encode($data['aliases']));

        $em->persist($keyword);
        $em->flush();

        $data = array(
            'tag'     => $keyword->getTag(),
            'aliases' => (array) json_decode($keyword->getAliases())
        );

        return new JsonModel($data);
    }

    public function update($id, $data)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy =  $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            //$this->getResponse()->setStatusCode(401);
            //return new JsonModel(array('error' => 'Unauthorized'));
        }

        $keyword = $em->getRepository('Application\Entity\Keyword')->getByTag($id);
        $keyword->setAliases(json_encode($data['aliases']));
        $em->persist($keyword);
        $em->flush();

        $data = array(
            'tag'     => $keyword->getTag(),
            'aliases' => (array) json_decode($keyword->getAliases())
        );

        return new JsonModel($data);
    }


    public function delete($id)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy =  $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $keyword = $em->getRepository('Application\Entity\Keyword')->getByTag($id);
        $em->remove($keyword);
        $em->flush();

        return new JsonModel(array('status' => "ok"));
    }
}
