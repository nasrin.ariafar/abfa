<?php
namespace Application\Controller;

use Application\Entity\Event;
use Application\Entity\EventResource;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class EventResourceController extends AbstractRestfulController
{
    public function create($data)
    {
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  =  $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $evId  = $this->params()->fromRoute('event_id', null);
        $event = $em->getRepository('Application\Entity\Event')->find($evId);
        if (!$event instanceof Event) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $event->setUpdateTime(new \DateTime('now'));

        $data['resources'] = isset($data['resources']) ? $data['resources'] : array();

        foreach ($data['resources'] as $row) {
            $resId = $row['id'];
            $desc  = isset($row['description']) ? $row['description'] : null;
            $count = isset($row['count']) ? $row['count'] : 1;

            $res = $em->getRepository('Application\Entity\Resource')->find($resId);
            if ($res) {
                $evres = new EventResource();
                $evres->setEvent($event);
                $evres->setResource($res);
                $evres->setDescription($desc);
                $evres->setCount($count);
                $em->persist($evres);
            }
        }

        $em->flush();

        $data = $this->extractEvent($event);
        $this->notifyUsers($event, $data, $this->identity());

        return new JsonModel(array('status' => 'ok'));
    }
}
