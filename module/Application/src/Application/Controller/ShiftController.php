<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\Shift,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    DateTime,
    Doctrine\Common\Annotations\AnnotationReader;

class ShiftController extends AbstractRestfulController
{
    public function getList()
    {
        $lastTime = $this->getRequest()->getQuery('lastUpdateTime', null);
        $start    = $this->params()->fromQuery('startDate', null);
        $end      = $this->params()->fromQuery('endDate', null);

        if ($start || $end) {
            $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $qb = $em->createQueryBuilder();
            $qb->select('s')->from('Application\Entity\Shift', 's');

            $and = $qb->expr()->andX();

            if ($start) {
                $start = new \DateTime($start);
                $and->add($qb->expr()->gte('s.date', ':start'));
                $qb->setParameter('start', $start);
            }

            if ($end) {
                $end = new \DateTime($end);
                $and->add($qb->expr()->lte('s.date', ':end'));
                $qb->setParameter('end', $end);
            }

            $qb->where($and);
            $qb->orderBy('s.date', 'ASC');
            $result = $qb->getQuery()->getResult();
        } elseif ($lastTime) {
            $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $qb = $em->createQueryBuilder();
            $qb->select('s')->from('Application\Entity\Shift', 's');
            $qb->where('s.updateDate > ?1');
            $qb->setParameter(1, new \DateTime($lastTime));
            $qb->orderBy('s.updateDate', 'DESC');
            $qb->setMaxResults(100);

            $result = $qb->getQuery()->getResult();

        } else {
            $result = $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager')
                ->getRepository("Application\Entity\Shift")
                ->findBy(array(), null, 24);
        }

        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $return   = array();
        foreach ($result as $row) {
            $_row = $this->extract($row);
            unset($_row['user']['region']);

            $return[] = $_row;
        }

        $now = new \DateTime('now');
        return new JsonModel(
            array(
                'items' => $return,
                'currentTime' => $now->format('Y-m-d H:i:s')
            )
        );
    }

    public function get($id)
    {
        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\Shift")
            ->find($id);

        if (!$result) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $result = $this->getServiceLocator()->get('Hydrator')->extract($result);
        unset($result['user']);
        return new JsonModel($result);
    }

    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder();
        $entity   = null;
        $user     = null;

        if (!empty($data['date'])) {
            $entity = $em->getRepository('Application\Entity\Shift')->findOneBy(array('date' => new DateTime($data['date'])));
        }

        if (!empty($data['user'])) {
            $user = $em->getRepository('Application\Entity\User')->find($data['user']);
        }

        $data['user'] = null;
        if ($user) {
            $data['user'] = $user;
        }

        if (!$entity) {
            $entity = new Shift();
        }

        $form = $builder->createForm($entity);
        $form->setHydrator($hydrator);
        $form->bind($entity);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();
        $entity->setCreationDate(new DateTime('now'));
        $entity->setUpdateDate(new DateTime('now'));

        $em->persist($entity);
        $em->flush();

        require_once "vendor/jdatetime.class.php";
        $user = $entity->getUser();
        $token = $user->getGcmToken();
        $phone = $user->getUsername();

        $jdate = \jDateTime::date('j F Y', $entity->getDate()->getTimestamp(), true);
        $jday  = \jDateTime::date('l', $entity->getDate()->getTimestamp(), true);
        $message = "جناب آقای " . $user->getFirstname() . ' ' . $user->getLastname() . "؛با سلا،جنابعالی به عنوان کشیک استان در روز %day% مورخ %date% تعیین گردیده‌اید. سامانه همراه مدیریت بحرانEMA";
        $message = str_replace('%day%', $jday, $message);
        $message = str_replace('%date%', $jdate, $message);

        if (!empty($token)) {
            $gcm = $this->getServiceLocator()->get('Application\Service\GcmService');
            $gcm->send(array('message' => $message), array($token));
        }

        if (!empty($phone)) {
            $sms = $this->getServiceLocator()->get('Application\Service\SendSms');
            $sms->send(array($phone), $message);
        }

        return new JsonModel($this->extract($entity));
    }

    public function update($id, $data)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $entity   = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\Shift")
            ->find($id);

        if (!$entity) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }


        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($entity);

        $form->setHydrator($hy);
        $form->bind($entity);

        $preData = $hy->extract($entity);
        unset($preData['user']);
        unset($data['date']);
        $data    = array_merge($preData, $data);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();
        $entity->setUpdateDate(new DateTime('now'));

        $em->persist($entity);
        $em->flush();

        require_once "vendor/jdatetime.class.php";
        $user  = $entity->getUser();
        $token = $user->getGcmToken();
        $phone = $user->getUsername();
        $jdate = \jDateTime::date('j F Y', $entity->getDate()->getTimestamp(), true);
        $jday  = \jDateTime::date('l', $entity->getDate()->getTimestamp(), true);
        $message = "جناب آقای " . $user->getFirstname() . ' ' . $user->getLastname() . "؛با سلا،جنابعالی به عنوان کشیک استان در روز %day% مورخ %date% تعیین گردیده‌اید. سامانه همراه مدیریت بحرانEMA";
        $message = str_replace('%day%', $jday, $message);
        $message = str_replace('%date%', $jdate, $message);

        $result = $hy->extract($entity);
        unset($result['user']);
        return new JsonModel($result);
    }

    public function delete($id)
    {
        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\Shift")
            ->find($id);

        if (!$result) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        try {
            $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->remove($result);
            $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->flush();
        } catch (\Exception $e) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('stats' => 'error'));
        }

        return new JsonModel(array('stats' => 'ok'));
    }
}
