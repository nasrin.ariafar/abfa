<?php
namespace Application\Controller;

use Application\Entity\Event;
use Application\Entity\EventCoordination;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class EventCommentController extends AbstractRestfulController
{
    public function getList()
    {
        $sm = $this->getServiceLocator();
        $em = $sm->get('Doctrine\ORM\EntityManager');
        $hy = $sm->get('Hydrator');
        $id = $this->params()->fromRoute('event_id', null);

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $event = $em->getRepository('Application\Entity\Event')->find($id);
        if (!$event instanceof Event) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $return = array();
        $result = $em->getRepository('Application\Entity\EventComment')
            ->findBy(array('event' => $event), array('creationDate' => 'ASC'));

        foreach($result as $row) {
            $return[] = $this->extract($row);
        }

        return new JsonModel($return);
    }

    public function create($data)
    {
        $sm  = $this->getServiceLocator();
        $em  = $sm->get('Doctrine\ORM\EntityManager');
        $hy  = $sm->get('Hydrator');
        $req = $this->getRequest();

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $evId  = $this->params()->fromRoute('event_id', null);
        $event = $em->getRepository('Application\Entity\Event')->find($evId);

        if (!$event instanceof Event) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $builder = new AnnotationBuilder();
        $entity  = new \Application\Entity\EventComment();
        $form    = $builder->createForm($entity);

        $form->setHydrator($hy);
        $form->bind($entity);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $event->setUpdateTime(new \DateTime('now'));

        $comment = $form->getData();
        $comment->setEvent($event);
        $comment->setUser($this->identity());
        $comment->setCreationDate(new \DateTime('now'));

        $em->persist($comment);
        $em->flush();

        $data = $this->extractEvent($event);
        $this->notifyUsers($event, $data, $this->identity());

        return new JsonModel($this->extract($comment));
    }
}
