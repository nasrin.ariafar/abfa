<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\ContactGroup,
    Application\Entity\Contact,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    Doctrine\Common\Annotations\AnnotationReader;

class ContactGroupUserController extends AbstractRestfulController
{
    protected function getGroup()
    {
        $groupId = $this->params()->fromRoute('group_id');
        return $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\ContactGroup")
            ->find($groupId);
    }

    public function getList()
    {
        $group = $this->getGroup();
        if (!$group) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $return = array();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        foreach ($group->getUser() as $u ) {
            $return[] = $this->extract($u);
        }

        return new JsonModel(array('users' => $return));
    }

    public function create($data)
    {
        $group = $this->getGroup();
        if (!$group) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        if (empty($data['user_id'])) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user     = $em->getRepository('Application\Entity\User')->find($data['user_id']);

        if (!$user) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $user->addContactGroup($group);
        $group->addUser($user);

        try {
            $em->flush($user);
        } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e) {
        }

        return new JsonModel(array("status" => "ok"));
    }

    public function delete($id)
    {
        $group = $this->getGroup();
        if (!$group) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $user = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\User")
            ->find($id);

        if (!$user) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        try {
            $user->removeContactGroup($group);
            $group->removeUser($user);

            $em->flush();
        } catch (\Exception $e) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('stats' => 'error'));
        }

        return new JsonModel(array('stats' => 'ok'));
    }
}
