<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Application\Entity\ExternalNews;
use Application\Entity\Source;
use Application\Entity\SourceLink;
use PicoFeed\Reader\Reader;

class FeedCrawlerController extends AbstractActionController
{
    public function crawlAction()
    {
        $request = $this->getRequest();
        $sources = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository('Application\Entity\SourceLink')
            ->findAll();

        foreach ($sources as $sourceLink) {
            $this->fetchFeed($sourceLink);
        }
    }

    protected function fetchFeed(SourceLink $sourceLink)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        try {
            $reader   = new Reader;
            $resource = $reader->download($sourceLink->getUrl());
            $parser   = $reader->getParser(
                $resource->getUrl(),
                $resource->getContent(),
                $resource->getEncoding()
            );

            $feed = $parser->execute();

            foreach ($feed->items as $item) {
                if ($this->isInserted($item->url)) {
                    continue;
                }

                $news = new ExternalNews();
                $news->setHeadline('');
                $news->setBody('');
                $news->setTitle($item->title);
                $news->setLead($item->content);
                $news->setCreationDate($item->date);
                $news->setLink($item->url);
                $news->setSource($sourceLink->getSource());

                $news->setCrawled(false);
                $news->setCrawling(false);

                $em->persist($news);
                $em->flush($news);
            }

            $sourceLink->setLastvisiteddate(new \DateTime('now'));
            $em->flush();

        } catch (Exception $e) {
            // Do something...
        }
    }

    protected function isInserted($url)
    {
        $rows = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository('Application\Entity\ExternalNews')
            ->findBy(array('link' => $url));

        return (count($rows) > 0);
    }
}
