<?php

namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    ZfcUserDoctrineORM\Options\ModuleOptions,
    Zend\View\Model\JsonModel,
    Zend\View\Model\ViewModel,
    Zend\Json\Json,
    Zend\Form\Annotation\AnnotationBuilder;

class UserRestController extends AbstractRestfulController
{

    public function loginAction()
    {
        $request = $this->getRequest();
        $form = $this->getServiceLocator()->get('zfcuser_login_form');

        if (!$request->isPost()) {
            //return new JsonModel(array('status' => 'error'));
        }

        if ($this->requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            $data = Json::decode($request->getContent(), $this->jsonDecodeType);
        } else {
            $data = $request->getPost()->toArray();
        }

        $form->setData($data);

        if (!$form->isValid()) {
            $messages = $form->getMessages();

            return new JsonModel(array('errors' => $messages));
        }

        // clear adapters
        $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
        $this->zfcUserAuthentication()->getAuthService()->clearIdentity();

        $adapter = $this->zfcUserAuthentication()->getAuthAdapter();
        $result = $adapter->prepareForAuthentication($this->getRequest());
        $auth = $this->zfcUserAuthentication()->getAuthService()->authenticate($adapter);

        if (!$auth->isValid()) {
            $adapter->resetAdapters();
            return new JsonModel(
                            array('error' => 'Authentication failed. Please try again.')
            );
        }

        $token = sprintf('%s-%s-%s', substr(sha1(microtime()), 0, 15), substr(md5(microtime()), 0, 15), uniqid());

        $user = $this->zfcUserAuthentication()->getAuthService()->getIdentity();
        $uToken = new \Application\Entity\UserToken();
        $uToken->setToken($token);
        $uToken->setUser($user);
        $uToken->setCreationDate(new \DateTime('now'));

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $em->persist($uToken);
        $em->flush();

        $hy = $this->getServiceLocator()->get('Hydrator');

        return new JsonModel($hy->extract($uToken));
    }

    public function verifyAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('status' => 'error'));
        }

        if ($this->requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            $data = Json::decode($request->getContent(), $this->jsonDecodeType);
        } else {
            $data = $request->getPost()->toArray();
        }

        $username = isset($data['phone']) ? $data['phone'] : null;
        $regCode = isset($data['code']) ? $data['code'] : null;
        $gcmToken = isset($data['gcm_token']) ? $data['gcm_token'] : null;

        $user = $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager')
                ->getRepository('Application\Entity\User')
                ->findOneBy(
                array('username' => $username)
        );

        if (!$user) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('status' => 'error'));
        }

        if ($user->getRegistrationCode() != $regCode) {
            $this->getResponse()->setStatusCode(404);

            return new JsonModel(array('status' => 'error'));
        }

        $user->setState(1);

        $token = sprintf('%s-%s-%s', substr(sha1(microtime()), 0, 15), substr(md5(microtime()), 0, 15), uniqid());

        $uToken = new \Application\Entity\UserToken();
        $uToken->setToken($token);
        $uToken->setUser($user);
        $uToken->setCreationDate(new \DateTime('now'));

        $user->setGcmToken($gcmToken);

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $em->persist($uToken);
        $em->flush();

        $hy = $this->getServiceLocator()->get('Hydrator');
        $data = $hy->extract($uToken);
        $data['user'] = $this->extract($user);
        $data['user']['levels'] = $this->extract($user->getLevel());

        return new JsonModel($data);
    }

    public function sendCodeAction()
    {
        $request = $this->getRequest();

        $phone = $request->getQuery('phone');

        $user = $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager')
                ->getRepository('Application\Entity\User')
                ->findOneBy(
                array('username' => $phone)
        );

        if (!$user) {
            $this->getResponse()->setStatusCode(404);

            return new JsonModel(array('status' => 'error'));
        }

        $registrationCode = substr(str_shuffle(str_shuffle("123456789")), 0, 5);
        //$registrationCode = "12345";

        $user->setRegistrationCode($registrationCode);

        $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager')
                ->flush();

        //send sms
        $message = "The E.M.A activation Code is: " . $registrationCode;
        $this->getServiceLocator()->get('Application\Service\SendSms')
                ->send($phone, $registrationCode);

        return new JsonModel(array('status' => 'ok'));
    }

    public function changePasswordAction()
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'inavlid user'));
        }

        $request = $this->getRequest();

        if ($this->requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            $data = Json::decode($request->getContent(), $this->jsonDecodeType);
        } else {
            $data = $request->getPost()->toArray();
        }

        $data['identity'] = $this->identity()->getUsername();

        $form = $this->getServiceLocator()->get('zfcuser_change_password_form');
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        if (!$this->getServiceLocator()->get('zfcuser_user_service')->changePassword($form->getData())) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'Error'));
        }

        return new JsonModel(array('status' => 'ok'));
    }

    public function getList()
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $result = array();
		$lastUpdate = $this->params()->fromQuery('lastUpdate', null);
		$updateDate = new \DateTime('now');

        foreach ($em->getRepository('Application\Entity\User')->findAllByLastUpdate($lastUpdate) as $row) {
            $user = $this->extract($row);
            $user['levels'] = $this->extract($row->getLevel());
            $user['region'] = $this->extract($row->getRegion());

            $result[] = $user;
        }

		if ($lastUpdate === null) {
			return new JsonModel($result);
		}

		return new JsonModel(array(
			'users' => $user,
			'updateDate' => $updateDate->format('Y-m-d H:i:s')
		));
	}

    public function get($id)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $user = $em->getRepository('Application\Entity\User')->find($id);

        if (!$user) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Invalid user'));
        }

        $data = $this->extract($user);
        $data['levels'] = $this->extract($user->getLevel());

        return new JsonModel($data);
    }

    public function create($data)
    {
        $id = $this->params('id');
        if (!empty($id)) {
            return $this->update($id, $data);
        }

        $request = $this->getRequest();
        $options = $this->getServiceLocator()->get('zfcuser_module_options');

        if (!$options->getEnableRegistration()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => 'disableRegistration'));
        }

        $registrationCode = substr(str_shuffle(str_shuffle("123456789")), 0, 5);

        $zfcServiceEvents = $this->getServiceLocator()
                        ->get('zfcuser_user_service')->getEventManager();

        $zfcServiceEvents->attach('register', function($e) use ($registrationCode) {
                    $form = $e->getParam('form');
                    $user = $e->getParam('user');

                    $user->setRegistrationCode($registrationCode);
                });

        $service = $this->getServiceLocator()->get('zfcuser_user_service');
        $form = $this->getServiceLocator()->get('zfcuser_register_form');
        $user = $service->register($data);

        if (!$user) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        if (isset($data['email'])) {
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(array('error' => array('email' => 'Invalid email')));
            }

            $user->setEmail($data['email']);
        }

        if (isset($data['position'])) {
            $user->setPosition($data['position']);
        }

        if (isset($data['type'])) {
            $user->setType($data['type']);
        }

        if (isset($data['firstname'])) {
            $user->setFirstname($data['firstname']);
        }

        if (isset($data['lastname'])) {
            $user->setLastname($data['lastname']);
        }

        if (isset($data['accessibility'])) {
            $user->setAccessibility($data['accessibility']);
        }

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        if (isset($data['region_id'])) {
            $region = $em->getRepository('Application\Entity\Region')->find($data['region_id']);
            if ($region) {
                $user->setRegion($region);
            }
        }

        $files = $this->getRequest()->getFiles()->toArray();
        if (count($files) > 0) {
            $file = array_shift($files);
            if (!$file['error']) {
                $validator = new \Zend\Validator\File\IsImage();
                if ($validator->isValid($file)) {
                    $image = $this->saveImage($file);
                    $user->setPhoto($image);

                    $this->getServiceLocator()
                            ->get('Doctrine\ORM\EntityManager')
                            ->flush($user);
                }
            }
        }

        $data['levels'] = isset($data['levels']) ? $data['levels'] : '';
        $data['levels'] = explode(',', $data['levels']);
        $data['levels'] = array_unique($data['levels']);
        foreach ($data['levels'] as $lid) {
            $level = $em->getRepository('Application\Entity\Level')->find($lid);

            if ($level) {
                $level->addUser($user);
                $user->addLevel($level);
            }
        }

		$user->setModifiedDate(new \DateTime('now'));
        $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager')
                ->flush();

        //TODO send sms
        $hy = $this->getServiceLocator()->get('Hydrator');
        $data = $hy->extract($user);
        $data['levels'] = $this->extract($user->getLevel());
        $data['region'] = $this->extract($user->getRegion());
        unset($data['password']);

        return new JsonModel($data);
    }

    public function update($id, $data)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $user = $em->getRepository('Application\Entity\User')->find($id);

        if (!$user) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Invalid user'));
        }

        if (isset($data['email'])) {
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(array('error' => array('email' => 'Invalid email')));
            }
            $user->setEmail($data['email']);
        }

        if (isset($data['position'])) {
            $user->setPosition($data['position']);
        }

        if (isset($data['type'])) {
            $user->setType($data['type']);
        }

        if (isset($data['firstname'])) {
            $user->setFirstname($data['firstname']);
        }

        if (isset($data['lastname'])) {
            $user->setLastname($data['lastname']);
        }

        if (isset($data['accessibility'])) {
            $user->setAccessibility($data['accessibility']);
        }


        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        if (isset($data['region_id'])) {
            $region = $em->getRepository('Application\Entity\Region')->find($data['region_id']);
            if ($region) {
                $user->setRegion($region);
            }
        }
        $files = $this->getRequest()->getFiles()->toArray();
        if (count($files) > 0) {
            $file = array_shift($files);
            if (!$file['error']) {
                $validator = new \Zend\Validator\File\IsImage();
                if ($validator->isValid($file)) {
                    $image = $this->saveImage($file);
                    $user->setPhoto($image);
                }
            }
        }

        $data['levels'] = isset($data['levels']) ? $data['levels'] : '';
        $data['levels'] = explode(',', $data['levels']);
        $data['levels'] = array_unique($data['levels']);
        foreach ($user->getLevel() as $level) {
            $level->removeUser($user);
        }

        foreach ($data['levels'] as $lid) {
            $level = $em->getRepository('Application\Entity\Level')->find(trim($lid));

            if ($level) {
                $level->addUser($user);
            }
        }

		$user->setModifiedDate(new \DateTime('now'));

        $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager')
                ->flush();

        return $this->get($user->getId());

        $data = $this->extract($user);
        $data['levels'] = $this->extract($user->getLevel());

        return new JsonModel($data);
    }

    public function delete($id)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $user = $em->getRepository('Application\Entity\User')->find($id);

        if (!$user) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Invalid user'));
        }

        try {
            $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->remove($user);
            $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->flush();
        } catch (\Exception $e) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('stats' => 'error'));
        }

        return new JsonModel(array('stats' => 'ok'));
    }

    protected function saveImage($file)
    {
        $imageName = substr(md5(microtime()), 0, 10);
        $imageName .= '-';
        $imageName .= md5(microtime() . $file['name']);
        $imageName .= '.jpg';
        $baseDir = realpath('public/');
        $directory = '/files/user-photos/'
                . substr($imageName, 0, 2) . '/' . substr($imageName, 2, 2);

        if (!is_dir($baseDir . DIRECTORY_SEPARATOR . $directory)) {
            mkdir($baseDir . DIRECTORY_SEPARATOR . $directory, 0777, true);
        }

        $path = $baseDir . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $imageName;
        move_uploaded_file($file['tmp_name'], $path);

        return $directory . '/' . $imageName;
    }

}
