<?php

namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    ZfcUserDoctrineORM\Options\ModuleOptions,
    Zend\View\Model\JsonModel,
    Zend\View\Model\ViewModel,
    Zend\Json\Json,
	Zend\Http\Client,
    Zend\Form\Annotation\AnnotationBuilder;

class Abfa122Controller extends AbstractRestfulController
{
    public function registerAction()
    {
        $user = $this->identity();
        if (!$user) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'error'));
        }

        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'error'));
        }

        if ($this->requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            $data = Json::decode($request->getContent(), $this->jsonDecodeType);
        } else {
            $data = $request->getPost()->toArray();
        }

		$client = new Client('http://192.168.108.11/abfa122/mobile/EMA.php', array(
			'maxredirects' => 0,
			'timeout'      => 30
		));
		$client->setMethod('POST');
		$client->setParameterPost(array(
			'user' => $data['username'],
			'pass' => $data['password'],
			'fn'   => 1
		));
		$response = $client->send();
		if (!$response->isSuccess()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'invalid response'));
		}
		//TODO check response

		$user = $this->identity();
        $em   = $this->getServiceLocator()
			->get('Doctrine\ORM\EntityManager');

		$accounts = $em->getRepository("Application\Entity\User122Account")->findAll(array('userId' => $user->getId()));
		foreach ($accounts as $account) {
			$em->remove($account);
		}

		$account = new \Application\Entity\User122Account();
		$account->setUserId($user->getId());
		$account->setUsername($data['username']);
		$account->setPassword($data['password']);
		$em->persist($account);
		$em->flush();

        return new JsonModel(array('status' => 'ok'));
    }

	public function getEventsAction()
	{
        $user = $this->identity();
        if (!$user) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'error'));
        }

        $em   = $this->getServiceLocator()
			->get('Doctrine\ORM\EntityManager');

		$row  = $em->getRepository('Application\Entity\User122Account')->findOneBy(array(
			'userId' => $user->getId()
		));

		if (!$row) {
			$this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'invalid user'));
		}

		$client = new Client('http://192.168.108.11/abfa122/mobile/EMA.php', array(
			'maxredirects' => 0,
			'timeout'      => 30
		));
		$client->setMethod('POST');
		$client->setParameterPost(array(
			'user' => $row->getUsername(),
			'pass' => $row->getPassword(),
			'fn'   => 2
		));
		$response = $client->send();
        echo $response->getContent();
        exit;
		$return   = json_decode($response->getContent(), true);

		return new JsonModel($return);
	}

	public function updateEventAction()
	{
        $user = $this->identity();
        if (!$user) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'error'));
        }

		$user = $this->identity();
        $em   = $this->getServiceLocator()
			->get('Doctrine\ORM\EntityManager');

		$row  = $em->getRepository('Application\Entity\User122Account')->findOneBy(array(
			'userId' => $user->getId()
		));

		if (!$row) {
			$this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'invalid user'));
		}

        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'invalid post request'));
        }

        if ($this->requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            $data = Json::decode($request->getContent(), $this->jsonDecodeType);
        } else {
            $data = $request->getPost()->toArray();
        }

		$client = new Client('http://192.168.108.11/abfa122/mobile/EMA.php', array(
			'maxredirects' => 0,
			'timeout'      => 30
		));
		$client->setMethod('POST');
		$client->setParameterPost(array(
			'user'  => $row->getUsername(),
			'pass'  => $row->getPassword(),
			'incId' => $data['eventId'],
			'type'  => 0,
			'fn'    => 3
		));
		$response = $client->send();
        echo $response->getContent();
        exit;
		$return   = json_decode($response->getContent(), true);

		return new JsonModel($return);
	}

    public function uploadPhotoAction()
    {
        $user = $this->identity();
        if (!$user) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'error'));
        }

		$user = $this->identity();
        $em   = $this->getServiceLocator()
			->get('Doctrine\ORM\EntityManager');

		$row  = $em->getRepository('Application\Entity\User122Account')->findOneBy(array(
			'userId' => $user->getId()
		));

		if (!$row) {
			$this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'invalid user'));
		}

        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'invalid post request'));
        }

        $eventId = $this->params()->fromQuery('eventId', null);
        if (empty($eventId)) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'invalid event id'));
        }

        $file = $this->getRequest()->getFiles('image');
        if (!$file) {
			$this->getResponse()->setStatusCode(400);
            return new JsonModel(array('status' => 'invalid file'));
        }

		$client = new Client('http://192.168.108.11/abfa122/mobile/EMA.php', array(
			'maxredirects' => 0,
			'timeout'      => 30
		));
		$client->setMethod('POST');
        $client->setFileUpload($file['tmp_name'], 'image');
        $client->setParameterPost(
            array(
                'user'  => $row->getUsername(),
                'pass'  => $row->getPassword(),
                'incId' => $eventId,
                'fn'    => 3
            )
        );
		$response = $client->send();
        echo $response->getContent();
        exit;
		$return   = json_decode($response->getContent(), true);

		return new JsonModel($return);
    }
}
