<?php
namespace Application\Controller;

use Application\Entity\User;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class UserPhotoController extends AbstractRestfulController
{
    public function create($data)
    {
        $sm  = $this->getServiceLocator();
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  = $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $uid  = $this->identity()->getId();
        $user = $em->getRepository('Application\Entity\User')->find($uid);

        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $files = $req->getFiles()->toArray();
        if (count($files) <= 0) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => 'invalid file'));
        }

        $file = array_shift($files);
        if ($file['error']) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => 'Error'));
        }

        $validator = new \Zend\Validator\File\IsImage();

        if (!$validator->isValid($file)) {
            return new JsonModel(array('error' => 'Error'));
        }

        $image = $this->saveImage($file);
        $user->setPhoto($image);
        $em->flush();

        return new JsonModel($this->extract($user));
    }

    public function deleteList()
    {
        $sm  = $this->getServiceLocator();
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  = $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $uid  = $this->params('user_id');
        $uid  = empty($uid) ? $this->identity()->getId() : $uid;
        $user = $em->getRepository('Application\Entity\User')->find($uid);

        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        @unlink($user->getPhoto());

        $user->setPhoto(null);
        $em->flush();

        return new JsonModel($this->extract($user));
    }

    protected function saveImage($file)
    {
        $imageName  = substr(md5(microtime()), 0, 10);
        $imageName .= '-';
        $imageName .= md5(microtime() . $file['name']);
        $imageName .= '.jpg';
        $baseDir    = realpath('public/');
        $directory  = '/files/user-photos/'
            . substr($imageName, 0, 2) . '/' . substr($imageName, 2, 2);

        if (!is_dir($baseDir . DIRECTORY_SEPARATOR . $directory)) {
            mkdir($baseDir . DIRECTORY_SEPARATOR . $directory, 0777, true);
        }

        $path = $baseDir . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $imageName;
        move_uploaded_file($file['tmp_name'], $path);

        return $directory . '/' . $imageName;
    }
}
