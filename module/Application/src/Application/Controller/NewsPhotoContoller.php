<?php
namespace Application\Controller;

use Application\Entity\User;
use Application\Entity\News;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class NewsPhotoController extends AbstractRestfulController
{
    public function getList()
    {
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  =  $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $nId  = $this->params()->fromRoute('news_id', null);
        $news = $em->getRepository('Application\Entity\News')->find($nId);

        if (!$news instanceof News) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $photos = $em->getRepository('Application\Entity\NewsPhoto')
            ->findBy(array('news' => $news));

        $ret = $this->extract($news);
        $ret['photos'] = array();

        foreach ($photos as $img) {
            $ret['photos'][] = $this->extract($img);
        }

        return new JsonModel($this->extract($photo));
    }

    public function create($data)
    {
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  =  $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $nId  = $this->params()->fromRoute('news_id', null);
        $news = $em->getRepository('Application\Entity\News')->find($nId);

        if (!$news instanceof News) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $news->setUpdateDate(new \DateTime('now'));

        $files = $req->getFiles()->toArray();
        foreach ($files as $file) {
            if ($file['error']) {
                return new JsonModel(array('error' => 'Error'));
            }

            $validator = new \Zend\Validator\File\IsImage();

            if (!$validator->isValid($file)) {
                return new JsonModel(array('error' => 'Error'));
            }

            $image = $this->saveImage($file);

            $photo = new NewsPhoto();
            $photo->setNews($entity);
            $photo->setImage($image);
            $photo->setUploadDate(new \DateTime('now'));

            $em->persist($photo);
        }

        $em->flush();

        return new JsonModel($this->extract($photo));
    }

    public function deleteList()
    {
        $sm  = $this->getServiceLocator();
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  = $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $nId  = $this->params()->fromRoute('news_id', null);
        $news = $em->getRepository('Application\Entity\News')->find($nId);

        if (!$news instanceof News) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $photos = $em->getRepository('Application\Entity\NewsPhoto')
            ->findBy(array('news' => $news));

        foreach ($photos as $img) {
            $em->remove($img);
        }

        $em->flush();

        return new JsonModel(array('stats' => 'ok'));
    }

    public function delete($id)
    {
        $sm  = $this->getServiceLocator();
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  = $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $nId  = $this->params()->fromRoute('news_id', null);
        $news = $em->getRepository('Application\Entity\News')->find($nId);

        if (!$news instanceof News) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $photo = $em->getRepository('Application\Entity\NewsPhoto')
            ->find($id);

        if ($photo) {
            $em->remove($photo);
        }

        $em->flush();

        return new JsonModel(array('stats' => 'ok'));
    }

    protected function saveImage($file)
    {
        $imageName  = substr(md5(microtime()), 0, 10);
        $imageName .= '-';
        $imageName .= md5(microtime() . $file['name']);
        $imageName .= '.jpg';
        $baseDir    = realpath('public/');
        $directory  = '/files/news/'
            . substr($imageName, 0, 2) . '/' . substr($imageName, 2, 2);

        if (!is_dir($baseDir . DIRECTORY_SEPARATOR . $directory)) {
            mkdir($baseDir . DIRECTORY_SEPARATOR . $directory, 0777, true);
        }

        $path = $baseDir . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $imageName;
        move_uploaded_file($file['tmp_name'], $path);

        return $directory . '/' . $imageName;
    }
}
