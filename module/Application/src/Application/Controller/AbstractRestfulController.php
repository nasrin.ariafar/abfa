<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController as ZendAbstractRestfulController,
    Application\Entity\User,
    Zend\View\Model\JsonModel,
    Doctrine\Common\Annotations\AnnotationReader;

class AbstractRestfulController extends ZendAbstractRestfulController
{
    protected function getIdentifier($routeMatch, $request)
    {
        $identifier = $this->getIdentifierName();
        $id = $routeMatch->getParam($identifier, false);
        if ($id !== false) {
            return $this->_explodeId($id);
        }

        $id = $request->getQuery()->get($identifier, false);
        if ($id !== false) {
            return $this->_explodeId($id);
        }

        return false;
    }

    private function _explodeId($id)
    {
        if (strpos($id, ',') === false) {
            return $id;
        }

        return explode(',', $id);
    }

    public function getList()
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    public function get($id)
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    public function create($data)
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    public function update($id, $data)
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    public function delete($id)
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    public function deleteList()
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    protected function extractEvent($event)
    {
        $em =  $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $this->extract($event);
        $data['readonly'] = (bool)$data['readonly'];

        $user = $event->getCreator();
        if ($user) {
            $user->getUsername();
            $data['creator'] = $this->extract($user);
        }

        $data['user'] = $data['creator'];

        $data['comments'] = array();
        $result = $em->getRepository('Application\Entity\EventComment')
            ->findBy(array('event' => $event), array('creationDate' => 'ASC'));

        foreach($result as $row) {
            $data['comments'][] = $this->extract($row);
        }

        $data['photos'] = array();
        $result = $em->getRepository('Application\Entity\EventPhoto')
            ->findBy(array('event' => $event));

        foreach($result as $row) {
            $data['photos'][] = $this->extract($row);
        }

        $data['resources'] = array();
        $result = $em->getRepository('Application\Entity\EventResource')
            ->findBy(array('event' => $event));

        foreach($result as $row) {
            $_data = $this->extract($row);
            unset($_data['event']);
            unset($_data['id']);
            $data['resources'][] = $_data;
        }

        $data['coordinations'] = array();
        $result = $em->getRepository('Application\Entity\EventCoordination')
            ->findBy(array('event' => $event));

        foreach($result as $row) {
            $_data = $this->extract($row);
            unset($_data['event']);
            unset($_data['id']);
            $data['coordinations'][] = $_data;
        }

        $data['users'] = array();
        foreach ($event->getUser() as $user) {
            $data['users'][] = $this->extract($user);
        }

        return $data;
    }

    protected function extract($obj)
    {
        if (!is_object($obj)) {
            return $obj;
        }

        if ($obj instanceof \DateTime) {
            return $obj;
        }

        $hy =  $this->getServiceLocator()->get('Hydrator');

        if ($obj instanceof \Doctrine\ORM\PersistentCollection) {
            $res = array();
            foreach ($obj->toArray() as $row) {
                $res[] = $this->extract($row);
            }

            return $res;
        }

        $data = $hy->extract($obj);

        if ($obj instanceof \Application\Entity\ExternalNews) {
            $data['text'] = strip_tags($data['body']);
            $data['body'] = preg_replace("/<script.*?>(.*)?<\/script>/im","$1", $data['body']);
            $data['body'] = preg_replace("/<iframe.*?>(.*)?<\/iframe>/im","$1", $data['body']);
        }

        if ($obj instanceof \Application\Entity\Keyword) {
            unset($data['aliases']);
        }

        if ($obj instanceof \Application\Entity\User) {
            unset($data['level']);
            unset($data['event']);
            unset($data['password']);
            unset($data['gcmToken']);
            unset($data['contactGroup']);
        }

        if ($obj instanceof \Application\Entity\Level) {
            unset($data['user']);
        }

        if ($obj instanceof \Application\Entity\EventComment) {
            unset($data['event']);
        }

        if ($obj instanceof \Application\Entity\NewsPhoto) {
            unset($data['news']);
        }

        if (!$obj instanceof User) {
            foreach ($data as $key => &$val) {
                $val = $this->extract($val);
            }
        }

        if ($obj instanceof \Application\Entity\Event) {
            $data['causes'] = $data['cause'];
            $data['types'] = $data['type'];
            unset($data['cause']);
            unset($data['type']);
        }

        return $data;
    }

    protected function notifyUser($event, $data, $users)
    {
        $ids    = array();
        $phones = array();

        foreach ($users as $user) {
            $phone = $row->getUsername();
            if (!empty($phone)) {
                $phones[] = $phone;
            }

            $token = $row->getGcmToken();
            if (!empty($token)) {
                $ids[] = $token;
            }
        }

        $ids = array_unique($ids);

        if (count($ids) > 0) {
            $gcm = $this->getServiceLocator()->get('Application\Service\GcmService');
            $gcm->send(array('event_id' => $event->getId()), $ids);
        }
    }

    protected function notifyUsers($event, $data, $user, $sendSms = false)
    {
        $ids    = array();
        if ($user) {
            $phones = array($user->getUsername());
        } else {
            $phones = array();
        }

        $level  = $event->getLevel();

        if ($level) {
            /*
            foreach ($level->getUser() as $row) {
                $phone = $row->getUsername();
                if (!empty($phone)) {
                    $phones[] = $phone;
                }

                $token = $row->getGcmToken();
                if (!empty($token)) {
                    $ids[] = $token;
                }
            }
             */
        }

        foreach ($event->getUser() as $row) {
            $token = $row->getGcmToken();
            if (!empty($token)) {
                $ids[] = $token;
            }

            $phone = $row->getUsername();
            if (!empty($phone)) {
                $phones[] = $phone;
            }
        }

        $ids = array_unique($ids);
        /*
        $indx = array_search($user->getGcmToken(), $ids);
        if ($indx !== false) {
            unset($ids[$indx]);
        }
         */

        if (count($ids) > 0) {
            $gcm = $this->getServiceLocator()->get('Application\Service\GcmService');
            $gcm->send(array('event_id' => $event->getId()), $ids);
        }

        if ($sendSms) {
            $phones = array_unique($phones);
            if (count($phones) > 0) {
                $types = array();
                foreach ($data['types'] as $c) {
                    $types[] = $c['title'];
                }
                $message  = 'با سلام,گزارش ';
                $message .= implode(' , ', $types);
                $message .= " در آدرس ";
                $message .= $data['location'];
                $message .= "\nسامانه همراه مدیریت بحرانEMA";

                $sms = $this->getServiceLocator()->get('Application\Service\SendSms');
                $sms->send($phones, $message);
            }
        }

        /*
        $phones = array_unique($phones);
        if (count($phones) > 0) {
            $sms = $this->getServiceLocator()->get('Application\Service\SendSms');
            $sms->send($phones, json_encode($data));
        }
         */
    }
}
