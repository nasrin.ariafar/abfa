<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\News,
    Application\Entity\NewsPhoto,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    Doctrine\Common\Annotations\AnnotationReader;

class ExternalNewsController extends AbstractRestfulController
{
    public function getList()
    {
        $lastTime = $this->getRequest()->getQuery('lastUpdateTime', null);
        $keyword  = $this->getRequest()->getQuery('keyword', null);
        $limit    = $this->getRequest()->getQuery('limit', 10);
        $before   = $this->getRequest()->getQuery('before', null);
        $after    = $this->getRequest()->getQuery('after', null);

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $qb = $em->createQueryBuilder();

        $qb->select('n')->from('Application\Entity\ExternalNews', 'n');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('n.crawled', true));
        $and->add($qb->expr()->eq('n.valid', true));
        $and->add($qb->expr()->eq('n.status', "'published'"));

        if (!empty($lastTime)) {
            $and->add($qb->expr()->gt('n.creationDate', new \DateTime($lastTime)));
        }

        if ($before) {
            $and->add($qb->expr()->lt('n.creationDate', ':before'));
            $qb->setParameter('before', $before);
        }

        if ($after) {
            $and->add($qb->expr()->gt('n.creationDate', ':after'));
            $qb->setParameter('after', $after);
        }

        $qb->where($and);
        $qb->orderBy('n.creationDate', 'DESC');
        $qb->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();
        $return = array();
        $update = new \DateTime('now');

        foreach ($result as $row) {
            $news  = $this->extract($row);
            if (!empty($news['image'])) {
                $news['image'] = $this->serverUrl($news['image']);
            }

            $update   = $row->getCreationDate();
            $return[] = $news;
        }

        $now = new \DateTime('now');

        return new JsonModel(
            array(
                'items' => $return,
                'updateTime'  => $update->format('Y-m-d H:i:s'),
                'currentTime' => $now->format('Y-m-d H:i:s')
            )
        );
    }

    public function get($id)
    {
        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository('Application\Entity\ExternalNews')
            ->find($id);

        if (!$result) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $news = $this->extract($result);
        if (!empty($news['image'])) {
            $news['image'] = $this->serverUrl($news['image']);
        }

        return new JsonModel($news);
    }

    public function publishAction()
    {
        $user = $this->identity();
        if (!$user->isAdmin()) {
            $this->getResponse()->setStatusCode(403);
            return new JsonModel(array('error' => 'Forbidden'));
        }

        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $id   = $this->params()->fromRoute('id');
        $news = $em->getRepository('Application\Entity\ExternalNews')->find($id);

        if (!$news) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $news->setStatus('published');
        $em->flush($news);

        $news = $this->extract($news);
        return new JsonModel($news);
    }

    public function unpublishAction()
    {
        $user = $this->identity();
        if (!$user->isAdmin()) {
            $this->getResponse()->setStatusCode(403);
            return new JsonModel(array('error' => 'Forbidden'));
        }

        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $id   = $this->params()->fromRoute('id');
        $news = $em->getRepository('Application\Entity\ExternalNews')->find($id);

        if (!$news) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $news->setStatus('unpublished');
        $em->flush($news);

        $news = $this->extract($news);
        return new JsonModel($news);
    }

    public function publishedAction()
    {
        return $this->newsListAction('published');
    }

    public function notProcessedAction()
    {
        return $this->newsListAction('not_processed');
    }

    public function unpublishedAction()
    {
        return $this->newsListAction('unpublished');
    }

    public function newsListAction($status)
    {
        $lastTime = $this->getRequest()->getQuery('lastUpdateTime', null);
        $keyword  = $this->getRequest()->getQuery('keyword', null);
        $limit    = $this->getRequest()->getQuery('limit', 10);
        $before   = $this->getRequest()->getQuery('before', null);
        $after    = $this->getRequest()->getQuery('after', null);

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $qb = $em->createQueryBuilder();

        $qb->select('n')->from('Application\Entity\ExternalNews', 'n');

        $date = new \DateTime("-2 months");
        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('n.crawled', true));
        $and->add($qb->expr()->eq('n.valid', true));
        $and->add($qb->expr()->eq('n.status', $qb->expr()->literal($status)));
        $and->add($qb->expr()->gt('n.creationDate', $qb->expr()->literal($date->format('Y-m-d H:i:s'))));

        if (!empty($lastTime)) {
            $and->add($qb->expr()->gt('n.creationDate', new \DateTime($lastTime)));
        }

        if ($before) {
            $and->add($qb->expr()->lt('n.creationDate', ':before'));
            $qb->setParameter('before', $before);
        }

        if ($after) {
            $and->add($qb->expr()->gt('n.creationDate', ':after'));
            $qb->setParameter('after', $after);
        }

        $qb->where($and);
        $qb->orderBy('n.creationDate', 'DESC');
        $qb->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();
        $return = array();
        $update = new \DateTime('now');

        foreach ($result as $row) {
            $news  = $this->extract($row);
            if (!empty($news['image'])) {
                $news['image'] = $this->serverUrl($news['image']);
            }

            $update   = $row->getCreationDate();
            $return[] = $news;
        }

        $now = new \DateTime('now');

        return new JsonModel(
            array(
                'items' => $return,
                'updateTime' => $update->format('Y-m-d H:i:s'),
                'currentTime' => $now->format('Y-m-d H:i:s')
            )
        );
    }
}
