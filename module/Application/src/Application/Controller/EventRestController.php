<?php
namespace Application\Controller;

use Application\Entity\Event;
use Application\Entity\EventCoordination;
use Application\Entity\EventResource;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class EventRestController extends AbstractRestfulController
{
    public function getList()
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy =  $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $user = $this->identity();
        $time = $this->getRequest()->getQuery('lastUpdate', '2002-02-01 1:2:2');
        $user->getLevel()->initialize();

        $return = array();

        /*
        $levels = array();
        foreach ($user->getLevel() as $l) {
            $levels[] = $l->getId();
        }


        if (!empty($levels)) {
            $qb  = $em->getRepository('Application\Entity\Event')->createQueryBuilder('e');
            $and = $qb->expr()->andX();
            $and->add(
                $qb->expr()->gt(
                    'e.updateTime',
                    '?1'
                )
            );
            $and->add($qb->expr()->in('l.id', $levels));

            $qb->leftJoin('e.level', 'l')->where($and);
            $qb->setParameter(1, date('Y-m-d H:i:s', strtotime($time)));

            $query = $qb->getQuery();
            foreach ($query->getResult() as $row) {
                if (!isset($return[$row->getId()])) {
                    $return[$row->getId()] = $this->extractEvent($row);
                }
            }
        }
         */


        /*
        $qb  = $em->getRepository('Application\Entity\Event')->createQueryBuilder('e');
        $and = $qb->expr()->andX();
        $and->add(
            $qb->expr()->gt(
                'e.updateTime',
                '?1'
            )
        );
        $and->add($qb->expr()->eq('e.region', '?2'));
        $qb->where($and);
        $qb->setParameter(1, date('Y-m-d H:i:s', strtotime($time)));
        $qb->setParameter(2, $user->getRegion());

        $query = $qb->getQuery();
        foreach ($query->getResult() as $row) {
            if (!isset($return[$row->getId()])) {
                $return[$row->getId()] = $this->extractEvent($row);
            }
        }
         */

        $qb  = $em->getRepository('Application\Entity\Event')->createQueryBuilder('e');
        $and = $qb->expr()->andX();
        $and->add(
            $qb->expr()->gt(
                'e.updateTime',
                '?1'
            )
        );
        $and->add($qb->expr()->eq('e.creator', '?2'));

        $qb->where($and);
        $qb->setParameter(1, date('Y-m-d H:i:s', strtotime($time)));
        $qb->setParameter(2, $user);

        $query = $qb->getQuery();
        foreach ($query->getResult() as $row) {
            if (!isset($return[$row->getId()])) {
                $return[$row->getId()] = $this->extractEvent($row);
            }
        }

        $qb  = $em->getRepository('Application\Entity\Event')->createQueryBuilder('e');
        $and = $qb->expr()->andX();
        $and->add(
            $qb->expr()->gt(
                'e.updateTime',
                '?1'
            )
        );
        $and->add($qb->expr()->eq('u.id', $user->getId()));

        $qb->leftJoin('e.user', 'u')->where($and);
        $qb->setParameter(1, date('Y-m-d H:i:s', strtotime($time)));

        $query = $qb->getQuery();
        foreach ($query->getResult() as $row) {
            if (!isset($return[$row->getId()])) {
                $return[$row->getId()] = $this->extractEvent($row);
            }
        }

        $data = array(
            'lastUpdate' => date('Y-m-d H:i:s', strtotime('now')),
            'items' => array_values($return)
        );

        return new JsonModel($data);
    }

    public function get($id)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy =  $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $event = $em->find('Application\Entity\Event', $id);

        if (!$event) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $data = $this->extractEvent($event);

        return new JsonModel($data);
    }

    public function create($data)
    {
        $sm = $this->getServiceLocator();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy =  $this->getServiceLocator()->get('Hydrator');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $builder = new AnnotationBuilder();
        $entity  = new Event();
        $form    = $builder->createForm($entity);

        $form->setHydrator($hy);
        $form->bind($entity);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();

        $region = $entity->getRegion();
        $lastFl = 'data/ids/' . $region->getCode();
        $lastId = is_file($lastFl) ? file_get_contents($lastFl) : 100;

        $lastId++;

        file_put_contents($lastFl, $lastId);

        $id = $region->getCode() . '-' . $lastId;

        $entity->setId($id);
        $entity->setReportTime(new \DateTime('now'));
        $entity->setUpdateTime(new \DateTime('now'));
        $entity->setCreator($this->identity());

        $em->persist($entity);

        $data['causes'] = isset($data['causes']) ? $data['causes'] : array();
        foreach ($data['causes'] as $cid) {
            $res = $em->getRepository('Application\Entity\Cause')->find($cid);
            if ($res) {
                $entity->addCause($res);
            }
        }

        $data['types'] = isset($data['types']) ? $data['types'] : array();
        foreach ($data['types'] as $cid) {
            $res = $em->getRepository('Application\Entity\Type')->find($cid);
            if ($res) {
                $entity->addType($res);
            }
        }

        $data['coordinations'] = isset($data['coordinations']) ? $data['coordinations'] : array();
        foreach ($data['coordinations'] as $row) {
            $resId = $row['id'];
            $desc  = isset($row['description']) ? $row['description'] : null;

            $res = $em->getRepository('Application\Entity\Coordination')->find($resId);

            if ($res) {
                $evres = new EventCoordination();
                $evres->setEvent($entity);
                $evres->setCoordination($res);
                $evres->setDescription($desc);
                $em->persist($evres);
            }
        }

        $data['resources'] = isset($data['resources']) ? $data['resources'] : array();
        foreach ($data['resources'] as $row) {
            $resId = $row['id'];
            $desc  = isset($row['description']) ? $row['description'] : null;
            $count = isset($row['count']) ? $row['count'] : 1;

            $res = $em->getRepository('Application\Entity\Resource')->find($resId);
            if ($res) {
                $evres = new EventResource();
                $evres->setEvent($entity);
                $evres->setResource($res);
                $evres->setDescription($desc);
                $evres->setCount($count);
                $em->persist($evres);
            }
        }

        $data['users'] = isset($data['users']) ? $data['users'] : array();
        foreach ($data['users'] as $uid) {
            $ouser = $em->getRepository('Application\Entity\User')->find($uid);

            if ($ouser) {
                $entity->removeUser($ouser);
                $entity->addUser($ouser);
            }
        }

        $em->flush();

        $data = $hy->extract($entity);
        $data['user'] = $this->extract($this->identity());
        $data['level'] = $hy->extract($data['level']);
        $data['region'] = $hy->extract($data['region']);
        $data['levels'] = array();
        $data['types'] = array();

        foreach ($entity->getCause()->toArray() as $row) {
            $data['causes'][] = $hy->extract($row);
        }

        foreach ($entity->getType()->toArray() as $row) {
            $data['types'][] = $hy->extract($row);
        }

        $this->notifyUsers($entity, $data, $this->identity(), true);

        return new JsonModel($data);
    }
}
