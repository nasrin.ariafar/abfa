<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\ContactGroup,
    Application\Entity\Contact,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    Doctrine\Common\Annotations\AnnotationReader;

class ContactController extends AbstractRestfulController
{
    public function getList()
    {
        $sm = $this->getServiceLocator();
        $em = $sm->get('Doctrine\ORM\EntityManager');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $return = array();
        $user   = $authService->getIdentity();
        $groups = $user->getContactGroup();

        $hydrator = $this->getServiceLocator()->get('Hydrator');
        foreach ($groups as $group) {
            $_group = $hydrator->extract($group);

            unset($_group['user']);

            $contacts = array();
            $rows = $em->getRepository("Application\Entity\Contact")->findBy(array('group' => $group));
            foreach ($rows as $row) {
                $_row = $hydrator->extract($row);
                unset($_row['group']);
                $contacts[] = $_row;

            }

            $_group['contacts'] = $contacts;
            $return['groups'][] = $_group;
        }

        return new JsonModel($return);
    }
}
