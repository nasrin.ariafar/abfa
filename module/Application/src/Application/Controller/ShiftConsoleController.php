<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Application\Entity\Shift,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    DateTime,
    Doctrine\Common\Annotations\AnnotationReader;

class ShiftConsoleController extends AbstractActionController
{
    public function sendNotificationAction()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $tommorow = new DateTime('+1 day');
        $entity = $em->getRepository('Application\Entity\Shift')->findOneBy(array('date' => $tommorow));

        if (!$entity) {
            return;
        }

        require_once "vendor/jdatetime.class.php";
        $user  = $entity->getUser();
        $token = $user->getGcmToken();
        $phone = $user->getUsername();
        $name  = $user->getFirstname() . ' ' . $user->getLastname();
        $jdate = \jDateTime::date('j F Y', $entity->getDate()->getTimestamp(), true);
        $day   = \jDateTime::date('l', $entity->getDate()->getTimestamp(), true);

        $message = "جناب آقای %name%؛ با سلام، جنابعالی به عنوان کشیک استان در روز %day% مورخ %date% تعیین گردیده اید.\nسامانه همراه مدیریت بحرانEMA";
        $message = str_replace('%date%', $jdate, $message);
        $message = str_replace('%day%', $day, $message);
        $message = str_replace('%name%', $name, $message);

        if (!empty($token)) {
            $gcm = $this->getServiceLocator()->get('Application\Service\GcmService');
            $gcm->send(array('message' => $message), array($token));
        }

        if (!empty($phone)) {
            $sms = $this->getServiceLocator()->get('Application\Service\SendSms');
            $sms->send(array($phone), $message);
        }

        return "Done";
    }
}
