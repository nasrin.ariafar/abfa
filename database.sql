-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 30, 2014 at 04:21 PM
-- Server version: 5.6.19
-- PHP Version: 5.4.32-1~dotdeb.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `abfa`
--

-- --------------------------------------------------------

--
-- Table structure for table `causes`
--

CREATE TABLE IF NOT EXISTS `causes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `description` text COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `causes`
--

INSERT INTO `causes` (`id`, `title`, `description`) VALUES
(1, 'ad', 'sd\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `coordinations`
--

CREATE TABLE IF NOT EXISTS `coordinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `coordinations`
--

INSERT INTO `coordinations` (`id`, `title`) VALUES
(1, 'asdas');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `report_time` datetime NOT NULL,
  `location` text COLLATE utf8_persian_ci,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `happening_time` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_region_id_fk` (`region_id`),
  KEY `events_level_id_fk` (`level_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `report_time`, `location`, `longitude`, `latitude`, `region_id`, `level_id`, `happening_time`, `user_id`) VALUES
('dd-101', '2014-08-30 13:23:43', NULL, NULL, NULL, 1, 1, '2012-02-03 00:00:00', 1),
('dd-102', '2014-08-30 13:23:49', NULL, NULL, NULL, 1, 1, '2012-02-03 00:00:00', 1),
('dd-106', '2014-08-30 15:43:14', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-107', '2014-08-30 15:43:34', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-108', '2014-08-30 15:44:17', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-109', '2014-08-30 15:44:31', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-110', '2014-08-30 15:44:41', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-111', '2014-08-30 15:50:50', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-112', '2014-08-30 15:51:08', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-113', '2014-08-30 15:52:01', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-114', '2014-08-30 15:52:20', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-115', '2014-08-30 15:52:53', NULL, NULL, NULL, 1, 1, '2012-03-07 00:00:00', 1),
('dd-1156', '2014-08-29 16:34:52', NULL, NULL, NULL, 1, 1, NULL, NULL),
('dd-1532', '2014-08-29 17:17:15', NULL, NULL, NULL, 1, 1, '2012-02-23 12:11:00', NULL),
('dd-2010', '2014-08-30 13:19:33', NULL, NULL, NULL, 1, 1, '2012-02-03 00:00:00', 1),
('dd-2034', '2014-08-30 13:20:12', NULL, NULL, NULL, 1, 1, '2012-02-03 00:00:00', 1),
('dd-2325', '2014-08-29 16:51:00', NULL, NULL, NULL, 1, 1, '2012-02-23 12:11:00', NULL),
('dd-2369', '2014-08-29 16:35:40', NULL, NULL, NULL, 1, 1, NULL, NULL),
('dd-330', '2014-08-29 17:18:00', 'LOC address', 32.323, 23.032323, 1, 1, '2012-02-23 12:11:00', NULL),
('dd-410', '2014-08-29 16:40:39', NULL, NULL, NULL, 1, 1, NULL, NULL),
('dd-507', '2014-08-29 16:51:47', NULL, NULL, NULL, 1, 1, '2012-02-23 12:11:00', NULL),
('dd-525', '2014-08-29 16:31:11', NULL, NULL, NULL, 1, 1, NULL, NULL),
('dd-700', '2014-08-29 16:30:44', NULL, NULL, NULL, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event_causes`
--

CREATE TABLE IF NOT EXISTS `event_causes` (
  `event_id` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `cause_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`,`cause_id`),
  KEY `cause_id` (`cause_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `event_causes`
--

INSERT INTO `event_causes` (`event_id`, `cause_id`) VALUES
('dd-1532', 1),
('dd-2325', 1),
('dd-330', 1),
('dd-410', 1),
('dd-507', 1);

-- --------------------------------------------------------

--
-- Table structure for table `event_comments`
--

CREATE TABLE IF NOT EXISTS `event_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_id` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `body` text COLLATE utf8_persian_ci,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `event_comments`
--

INSERT INTO `event_comments` (`id`, `user_id`, `event_id`, `body`, `creation_date`) VALUES
(1, 1, 'dd-101', NULL, '2014-08-30 16:14:14'),
(2, 1, 'dd-101', NULL, '2014-08-30 16:14:31'),
(3, 1, 'dd-101', NULL, '2014-08-30 16:15:21'),
(4, 1, 'dd-101', 'salam', '2014-08-30 16:16:09'),
(5, 1, 'dd-101', 'salam', '2014-08-30 16:16:31');

-- --------------------------------------------------------

--
-- Table structure for table `event_coordinations`
--

CREATE TABLE IF NOT EXISTS `event_coordinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `coordination_id` int(11) NOT NULL,
  `description` text COLLATE utf8_persian_ci,
  PRIMARY KEY (`id`),
  KEY `coordination_id` (`coordination_id`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_photos`
--

CREATE TABLE IF NOT EXISTS `event_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `upload_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_resources`
--

CREATE TABLE IF NOT EXISTS `event_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `resource_id` int(11) NOT NULL,
  `description` text COLLATE utf8_persian_ci,
  `count` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`),
  KEY `resource_id` (`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_types`
--

CREATE TABLE IF NOT EXISTS `event_types` (
  `event_id` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`,`type_id`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE IF NOT EXISTS `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `description` text COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `title`, `description`) VALUES
(1, 'fd', 'fds');

-- --------------------------------------------------------

--
-- Table structure for table `level_users`
--

CREATE TABLE IF NOT EXISTS `level_users` (
  `level_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`level_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `code` varchar(10) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `title`, `code`) VALUES
(1, 'fds', 'dd');

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE IF NOT EXISTS `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `title`, `category_id`) VALUES
(2, 'sdfs', 1),
(3, 'sfds', 2);

-- --------------------------------------------------------

--
-- Table structure for table `resource_categories`
--

CREATE TABLE IF NOT EXISTS `resource_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `resource_categories`
--

INSERT INTO `resource_categories` (`id`, `title`) VALUES
(1, 'sdf'),
(2, 'sdfd');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `type_categories`
--

CREATE TABLE IF NOT EXISTS `type_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `state` smallint(6) DEFAULT NULL,
  `registration_code` int(10) DEFAULT NULL,
  `gcm_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `display_name`, `password`, `state`, `registration_code`, `gcm_token`) VALUES
(1, '09367111317', 'alireza.meskin@gmail.com', NULL, '$2y$14$ThG/XLjM4yW5wrfaOdHEb.AUA4XGKzMEWCqUnWWv3O/RQ8ZKJ4OuG', 1, 29378, '423242nsfdsfsffds3424'),
(4, '09121112345', 'meskin@gmail.com', NULL, '$2y$14$JJPWO7AP9u7nKyqPd9TgsesXw2NzxPkOZ0q602c2aRhfQga0lseAm', 1, 15948, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_tokens`
--

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_tokens`
--

INSERT INTO `user_tokens` (`id`, `user_id`, `token`, `creation_date`) VALUES
(1, 1, '7681b0aa706ca70-659211399990aed-5401815f1cfbc', '2014-08-30 12:16:39'),
(2, 1, '50a3383a4be2c70-ad8f328b9f3f88a-5401a39c52f03', '2014-08-30 14:42:44'),
(3, 1, '3f37f1061fa6853-7e4ca1381f88e7e-5401a3a441e91', '2014-08-30 14:42:52'),
(4, 1, 'a79721e3a508c75-42c9664bbdda5fe-5401a3adb76f4', '2014-08-30 14:43:01'),
(5, 1, '94c7bcdfbbaa2f4-dd69aea3cf0f1d0-5401a3c05a7d5', '2014-08-30 14:43:20');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `events_level_id_fk` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`),
  ADD CONSTRAINT `events_region_id_fk` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`);

--
-- Constraints for table `event_causes`
--
ALTER TABLE `event_causes`
  ADD CONSTRAINT `event_causes_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `event_causes_ibfk_1` FOREIGN KEY (`cause_id`) REFERENCES `causes` (`id`);

--
-- Constraints for table `event_comments`
--
ALTER TABLE `event_comments`
  ADD CONSTRAINT `event_comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `event_comments_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`);

--
-- Constraints for table `event_coordinations`
--
ALTER TABLE `event_coordinations`
  ADD CONSTRAINT `event_coordinations_ibfk_1` FOREIGN KEY (`coordination_id`) REFERENCES `coordinations` (`id`),
  ADD CONSTRAINT `event_coordinations_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`);

--
-- Constraints for table `event_photos`
--
ALTER TABLE `event_photos`
  ADD CONSTRAINT `event_photos_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`);

--
-- Constraints for table `event_resources`
--
ALTER TABLE `event_resources`
  ADD CONSTRAINT `event_resources_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `event_resources_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`);

--
-- Constraints for table `event_types`
--
ALTER TABLE `event_types`
  ADD CONSTRAINT `event_types_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `event_types_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`);

--
-- Constraints for table `level_users`
--
ALTER TABLE `level_users`
  ADD CONSTRAINT `level_users_ibfk_1` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`),
  ADD CONSTRAINT `level_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `resources`
--
ALTER TABLE `resources`
  ADD CONSTRAINT `resources_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `resource_categories` (`id`);

--
-- Constraints for table `types`
--
ALTER TABLE `types`
  ADD CONSTRAINT `types_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `type_categories` (`id`);

--
-- Constraints for table `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;





CREATE TABLE IF NOT EXISTS `news` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
    `short_desc` text COLLATE utf8_persian_ci,
    `long_desc` longtext COLLATE utf8_persian_ci,
    `creation_date` datetime NOT NULL,
    `update_date` datetime DEFAULT NULL,
    `deleted` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=10 ;

    CREATE TABLE IF NOT EXISTS `news_photos` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `news_id` int(11) NOT NULL,
        `image` varchar(255) NOT NULL,
        `upload_date` datetime NOT NULL,
        PRIMARY KEY (`id`),
        KEY `news_id` (`news_id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

        ALTER TABLE `news_photos`
        ADD CONSTRAINT `news_photos_ibfk_1` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`);
