define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'collections/shifts',
    'text!views/templates/calendar.html',
    'text!views/templates/add_shift.html',
    'i18n!nls/fa-ir/labels',
//    'pwt-date',
    'persianDate',
    'vendor/pwt.datepicker-master/src/js/mousewheel',
    'vendor/pwt.datepicker-master/src/js/plugin',
    'vendor/pwt.datepicker-master/src/js/constant',
    'vendor/pwt.datepicker-master/src/js/config',
    'vendor/pwt.datepicker-master/src/js/template',
    'vendor/pwt.datepicker-master/src/js/base-class',
    'vendor/pwt.datepicker-master/src/js/compat-class',
    'vendor/pwt.datepicker-master/src/js/helper',
    'vendor/pwt.datepicker-master/src/js/monthgrid',
    'vendor/pwt.datepicker-master/src/js/monthgrid-view',
    'vendor/pwt.datepicker-master/src/js/datepicker-view',
    'vendor/pwt.datepicker-master/src/js/datepicker',
    'vendor/pwt.datepicker-master/src/js/navigator',
    'vendor/pwt.datepicker-master/src/js/daypicker',
    'vendor/pwt.datepicker-master/src/js/monthpicker',
    'vendor/pwt.datepicker-master/src/js/yearpicker',
    'vendor/pwt.datepicker-master/src/js/toolbox',
    'vendor/pwt.datepicker-master/src/js/timepicker',
    'vendor/pwt.datepicker-master/src/js/state',
//     'moment',
//    'moment-timezone',
//    'moment-jalaali',
//    'fullcalendar'
], function($, _, Backbone, BaseView, ShiftsCollection, CalendarTpl, ShiftFormTpl, Labels) {

    var ItemsListView = BaseView.extend({
        className: "calendar-wrapper",
        initialize: function(options) {
            _.bindAll(this, 'render', 'hidePopover', 'clickOnADay', 'showEventSummaryPopover', 'loadCalendarEvents', 'onChangeMonth');

            this.options = options;
            this.shiftsCollection = new ShiftsCollection([]);
            this.shiftsCollection.on("add", this.renderUser, this);

            this.shiftsCollection.filters.set({
                'startDate': moment("2010-01-01").format("YYYY-MM-DD"),
                'endDate': moment("2100-01-01").format("YYYY-MM-DD")
            });
            this.shiftsCollection.fetch({
                async: false
            });

            return this;
        },
        reset: function() {
            _.each(this.items, function(itemView) {
                itemView.dispose();
            });

            this.items = [];
        },
        render: function(fields) {
            var $this = this;
            var tpl = _.template(CalendarTpl);
            this.$el.html(tpl());

            this.afterRender();
            return;


            // this.setScroll(this);
            var heightCalendar = $(window).height() - 66;

            this.$el.fullCalendar({
                height: heightCalendar,
                handleWindowResize: true,
                windowResize: function() {
                    $('#calendar-wrapper').fullCalendar('option', 'height', $(window).height() - 66);
                    $('#calendar-wrapper').fullCalendar('render');
                },
                editable: true,
                selectable: true,
                selectHelper: true,
                defaultView: 'month',
                allDaySlot: true,
//                eventDragStart: this.eventDragStart,
                eventLimit: true,
                fixedWeekCount: false,
                defaultDate: new Date(),
//                titleFormat: this.getFormatTitle(),
                select: $this.clickOnADay,
                unselectAuto: false,
                nextDayThreshold: "00:00:00",
                isRTL: true,
//                monthNames: ['فروردين', 'ارديبهشت', 'خرداد', 'تير', 'مرداد', 'شهريور',
//                    'مهر', 'آبان', 'آذر', 'دي', 'بهمن', 'اسفند'],
                monthNamesShort: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
                dayNames: ['يکشنبه', 'دوشنبه', 'سه‌شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه', 'شنبه'],
                dayNamesShort: ['يکشنبه', 'دوشنبه', 'سه‌شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه', 'شنبه'],
                dayNamesMin: ['يکشنبه', 'دوشنبه', 'سه‌شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه', 'شنبه'],
                weekHeader: 'هفته',
                dateFormat: 'yy/mm/dd',
                buttonText: {
                    'today': 'امروز',
                    'month': 'ماه',
                    'week': 'هفته',
                    'day': 'روز'
                },
                firstDay: 6,
                unselect: function() {
                    $(".popover-toggle").popover("destroy");
                },
                axisFormat: 'H(:mm)',
                timezone: "Asia/Tehran",
                timezoneParam: "Asia/Tehran",
                eventClick: $this.showEventSummaryPopover,
//                eventAfterRender: $this.eventAfterRender,
                header: {
                    left: '',
                    center: 'prev title next',
                    right: 'month,agendaWeek,agendaDay'
                },
//                lang: "fa",
                eventRender: function(event, el) {
                    var a = moment(event.start_date);
                    var b = moment(event.end_date);
                    if (a.diff(b, 'days') == 0) {
                        $(el).css('background-color', 'transparent');
                    }
                },
                viewRender: function(view, element) {
                    var start_date = view.start._d.getTime(),
                        end_date = view.end._d.getTime();


//                    $this.shiftsCollection.filters.set({
//                        'start_date': start_date,
//                        'end_date': end_date
//                    });
                    $this.loadCalendarEvents();
                    $('.fc-today').closest('.fc-content-skeleton').addClass('fc-week-selected');
                }
            });

//            $('.fc-toolbar').prepend(this.toolbarView.$el);
//            $('.fc-center h2').datepicker({
//                format: App.Prefs.User.profile.dateFormat.toLowerCase(),
//                autoclose: true,
//                todayBtn: "linked",
//                language: Application.Prefs.User.user_locale
//            });

            $('.fc-center').on('changeDate', function(event) {
                $('#calendar-wrapper').fullCalendar('gotoDate', event.date);

            });

        },
        afterRender: function() {
            var $this = this;
            window.pd = $("#inlineDatepicker").persianDatepicker({
                timePicker: {
                    enabled: true
                },
                dayPicker: {
                    scrollEnabled: false,
                    onSelect: $this.clickOnADay
                },
                navigator: {
                    onSwitch    : $this.onChangeMonth,
                    onNext      : $this.onChangeMonth,
                    onPrev      : $this.onChangeMonth
                },
                toolbox: {
                    onToday: $this.hidePopover
                },


//                altField: '#inlineDatepickerAlt',
                altFormat: "YYYY/MM/DD",
//                checkDate: function(unix) {
//                    var output = true;
//                    var d = new persianDate(unix);
//                    if (d.date() == 20) {
//                        output = false;
//                    }
//                    return output;
//                },
//                checkMonth: function(month) {
//                    var output = true;
//                    if (month == 1) {
//                        output = false;
//                    }
//                    return output;
//
//                }, checkYear: function(year) {
//                    var output = true;
//                    if (year == 1396) {
//                        output = false;
//                    }
//                    return output;
//                }

            }).data('datepicker');
        },

        onChangeMonth : function(view, elem){
            this.hidePopover();

        },
        hidePopover: function() {
            $('.popover-toggle').popover('destroy');
            $(".popover").remove();
            this.showShifts()
        },
        showShifts: function() {
            var $this = this;
            this.shiftsCollection.each(function(model) {
                $this.renderUser(model);
            });
        },
        renderUser: function(model) {
            var timestamp = moment(model.get("date"))._d.getTime(),
                el = $("[unixdate= " + timestamp + "]");
            if (el.length) {
                var user = model.get("user");
                el.append("<a class='user-info' id=" + model.get("id") + ">" + user.firstname + ' ' + user.lastname + "</a>")
            }
        },
        clickOnADay: function(start, calendar) {
            this.showShifts();

//            if (calendar.currentView == "day") {
            var $this = this,
                el = $(".table-days .selected");

//            if (!$(".table-days .selected .user-info").length) {
            el.popover({
                content: function() {
                    var hasShift = false,
                        model;
                    if ($(".table-days .selected .user-info").length) {
                        var modelId = $(".table-days .selected .user-info").attr("id");
                        model = $this.shiftsCollection.get(modelId);
                        hasShift = true;
                    }
                    var tpl = _.template(ShiftFormTpl);
                    return tpl({
                        labels: Labels,
                        hasShift: hasShift,
                        data: model
                    });
                },
                container: 'body',
                placement: "auto",
                html: true,
                viewport: {selector: 'body', padding: 0, width: "410px"}
            });

            el.on('show.bs.popover', function() {
                $(".popover").remove();
                el.addClass("popover-toggle");
            });

            el.on('shown.bs.popover', function() {
                $('.popover .cancel').off().on('click', function(e) {
                    $('.popover-toggle').popover('destroy');
                });

                $('.popover .add-shift').off().on('click', function(e) {
                    var model = new $this.shiftsCollection.model();
                    model.save({
                        user: $(".popover #user_id").val(),
                        date: moment(start).format("YYYY-MM-DD")
                    }, {
                        success: function(model) {
                            $this.shiftsCollection.add(model);
                            $('.popover-toggle').popover('destroy');
                        }
                    });
                });

                $('.popover .update-shift').off().on('click', function(e) {
                    var model = $this.shiftsCollection.get($(e.target).data("id"));
                    if (model.get("user").id != $(".popover #user_id").val()) {
                        model.save({
                            user: $(".popover #user_id").val(),
                            date: moment(start).format("YYYY-MM-DD")
                        }, {
                            success: function(model) {
                                var user = $(".popover #user_id").data("user");
                                model.set("user", user);
                                $(".user-info", el).html(user.firstname + ' ' + user.lastname);
                                $('.popover-toggle').popover('destroy');
                            }
                        });
                    } else {
                        $('.popover-toggle').popover('destroy');
                    }
                });

                $('.popover input').autocomplete({
                    source: window.usersCollection.toJSON(),
                    select: function(event, ui) {
                        $(".popover #user_id").data("user", ui.item);
                        $(".popover #user_id").val(ui.item.id);
                    },
                    minLength: 1,
                    appendTo: '#autocomplete-wrapper',
                });

            });

            el.on('hide.bs.popover', function() {
                $('.popover').hide();
                el.removeClass("popover-toggle");
            });

            el.popover('show');
//            }

//            }
        },
        showEventSummaryPopover: function() {

        },
        loadCalendarEvents: function(opt, callee) {
            var $this = this,
                calendar_filter = (opt && opt.calendar_filter) ? opt.calendar_filter : this.shiftsCollection.filters.toJSON();

            this.shiftsCollection.reset();
            this.shiftsCollection.filters.set(calendar_filter);

            $('#calendar-wrapper').fullCalendar('removeEvents');

            this.shiftsCollection.fetch({
                success: function(model) {
                    $('.fc-event-container').remove();
                    var events = [];

                    $this.shiftsCollection.each(function(model, index) {
                        events.push(model.toJSON());
                    });
                    $('#calendar-wrapper').fullCalendar('addEventSource', events);
                }
            });
        },
    });
    return ItemsListView
});
