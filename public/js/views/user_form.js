
define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'text!views/templates/user_form.html',
    'collections/levels',
    'collections/regions',
    'models/user',
    'i18n!nls/labels',
    'jquery-helper',
    'tagit'

    ], function ($, _, Backbone, BaseView, FormTpl, LevelsCollection, RegionsCollection, UserModel, Labels, Tagit) {

        var ItemView = BaseView.extend({
            
            className : "modal-dialog user-form",
            
            events : {
                'click #image_upload_btn'       : 'openSelectionFile',
                'change #file'                  : 'setImage',
                'click .create-item'            : 'createUser',
                'click .update-item'            : 'createUser'
            },
            
            initialize : function() {
                if(!this.model){
                    this.model = new UserModel();
                }
                this.levelsCollection = new LevelsCollection([]);
                this.regionsCollection = new RegionsCollection([]);
                this.levelsCollection.fetch({
                    async : false
                });
                this.regionsCollection.fetch({
                    async : false
                });
            },

            render : function() {
                this.template = _.template(FormTpl);
                this.$el.html(this.template(_.extend(this.model.toJSON(), {
                    labels  : Labels,
                    regions : this.regionsCollection.toJSON()
                })));
                return this;
            },

            afterRender : function(){
                var availableTags = []
                ,initTags = [];

                this.levelsCollection.each(function(item){
                    availableTags.push({
                        label : item.get("title"),
                        value : item.get("id")
                    })
                });

                if(this.model.get('levels') && this.model.get('levels').length ){
                    _.each(this.model.get('levels'), function(item){
                        initTags.push({
                            label : item.title,
                            value : item.id
                        });

                        availableTags = $.grep(availableTags, function(opt ,index){
                            return !( item.id == opt.value);
                        });
                    })
                }

                $('#display-levels').tagit({
                    initialTags     : initTags,
                    tagSource       : function(request, response){
                        var re = new RegExp(request.term , "i");
                        var array = $.grep(availableTags, function(opt ,index){
                            return (re.test(opt.label));
                        });
                        response (array);
                    },
                    sortable        : true,
                    allowNewTags    : false,
                    minLength       : 0,
                    tagsChanged : function(tagValue, action, element){
                        if(action == "added"){
                            $('#disply-fields-div').hasClass("error") && $('#disply-fields-div').removeClass("error");
                            availableTags = $.grep(availableTags, function(opt ,index){
                                return !(opt.label == tagValue);
                            });
                        }

                        if(action == "popped")
                            availableTags.push({
                                label : element.label,
                                value : element.value
                            })
                    }
                });
            },

            setImage : function(e){
                var input = $('#file')[0];

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.user-form .user-image').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            },
            openSelectionFile : function(){
                $("#file").trigger("click");
            },


            validate : function(){
                var validate =  true;

                $(".active form [required]").each(function(index, el){
                    if($(el).val() == ""){
                        validate =  false;
                        $(el).parent().addClass("error");
                    }
                });
                
                if($('#display-levels', this.$el).tagit("tags").length == 0){
                    validate =  false;
                    $('#display-levels', this.$el).parent().addClass("error");
                }
                !validate && $(".rquired_error").css("visibility", "visible");
                return validate;
            },

            createUser : function(){
                $(".error").removeClass("error");
                $(".rquired_error").css("visibility", "hidden");
                $(".error_msg").css("visibility", "hidden");
                
                if(!this.validate())
                    return;

                $(".modal-footer .btn:not(.btn-default)").attr("disabled", true);
                
                var data =  _.extend(this.options.model.toJSON(), $("form", this.$el).serializeJSON()),
                levels = "",
                $this = this,
                mode = this.options.model.get("id") ? "edit" : "create";

                data.displayName =  data.firstname + " " + data.lastname;
                var fd = new FormData();
                fd.append( 'file', $('#file')[0].files[0] );
                for(key in data){
                    if (data[key])
                        fd.append(key, data[key]);
                }

                var tags = $('#display-levels', this.$el).tagit("tags");
                if(tags.length){
                    _.each(tags, function(tag, index){
                        levels += tag.value + ",";;
                    });
                    
                    fd.append('levels', levels.replace(/,+$/, ""));
                }

                $.ajax({
                    type: "POST",
                    url: "/api/users" + ( (this.options.model && this.options.model.get("id")) ? "/" + this.options.model.get("id") : '') ,
                    processData: false,
                    contentType: false,
                    data: fd,
                    success: function (obj) {
                        if(mode == "edit")
                            $this.options.model.set(obj);
                        else
                            $this.collection.add(obj);
                        $("#new-item-modal").modal('hide');
                    },
                    error : function(error){
                        $('.hold-on-loading').hide();
                        $(".modal-footer .btn:not(.btn-default)").attr("disabled", false);
                        for (key in error){
                            var el = $("[name=" + key + "]").parents(".control-group");
                            el.addClass("error");
                            $("span", el).css("visibility", "visible");
                        }
                    }
                });

                return false;
            }
        });

        return ItemView;
    });
