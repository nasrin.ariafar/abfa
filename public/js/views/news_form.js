
define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'text!views/templates/news_form.html',
    'models/news',
    'i18n!nls/labels',
    'jquery-helper',
    ], function ($, _, Backbone, BaseView, FormTpl, NewsModel, Labels, Tagit) {

        var ItemView = BaseView.extend({
            
            className : "modal-dialog news-form",
            
            events : {
                'click #image_upload_btn'       : 'openSelectionFile',
                'change #file'                  : 'previewImages',
                'click .create-item'            : 'createNews',
                'click .delete-photo'           : 'deletePhoto'
            },
            
            initialize : function(options) {
                this.fd = new FormData();
                this.files = [];
                if(!this.model){
                    this.model = new NewsModel();
                }
            },

            render : function() {
                this.template = _.template(FormTpl);
                this.$el.html(this.template(_.extend(this.model.toJSON(), {
                    labels  : Labels
                })));
                return this;
            },

            setImage : function(e){
                var input = $('#file')[0];
                
                if (input.files) {
                    _.each(input.files, function(file, index){
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('.news-form .photos-list').append('<div><i class="glyphicon glyphicon-remove"></i><img src="'+ file +'"/></div>');
                        }
                        reader.readAsDataURL(input.files[0]);
                    })
                }
            },

            previewImages : function(e){
                var input = $('#file')[0],
                $this = this,
                fileList = input.files;
                
                $.each(fileList , function(index, file) {
                    $this.files.push(file)
                });
                
                var anyWindow = window.URL || window.webkitURL;

                for(var i = 0; i < fileList.length; i++){
                    //get a blob to play with
                    var objectUrl = anyWindow.createObjectURL(fileList[i]);
                    // for the next line to work, you need something class="preview-area" in your html
                    $('.news-form .photos-list').append('<div data-name="'+ fileList[i].name +'" class="photo-item"><span class="delete-photo">×</span><img src="' + objectUrl + '" /></div>');
                    // get rid of the blob
                    
                    window.URL.revokeObjectURL(fileList[i]);
                }

                if(this.model.get("id")){
                    
                    $.each(this.files , function(index, file) {
                        $this.fd.append('files['+index+']', file);
                    });

                    $.ajax({
                        type: "POST",
                        url: "/api/news/" + $this.model.get("id") + "/photos",
                        processData: false,
                        contentType: false,
                        data: $this.fd,
                        success: function (resp) {
                            var photos = $this.model.get("photos");
                            var newPhotos = photos.concat(resp);
                            $this.model.set("photos", newPhotos);
                        },
                        error : function(error){
                            $('.hold-on-loading').hide();
                            $(".modal-footer .btn:not(.btn-default)").attr("disabled", false);
                            for (key in error){
                                var el = $("[name=" + key + "]").parents(".control-group");
                                el.addClass("error");
                                $("span", el).css("visibility", "visible");
                            }
                        }
                    });
                }


            },

            openSelectionFile : function(){
                $("#file").trigger("click");
            },

            validate : function(){
                var validate =  true;

                $(".active form [required]").each(function(index, el){
                    if($(el).val() == ""){
                        validate =  false;
                        $(el).parent().addClass("error");
                    }

                });

                !validate && $(".rquired_error").css("visibility", "visible");
                return validate;
            },

            createNews : function(){
                
                $(".error").removeClass("error");
                $(".rquired_error").css("visibility", "hidden");
                
                if(!this.validate())
                    return;

                $(".modal-footer .btn:not(.btn-default)").attr("disabled", true);
                var data =  $("form", this.$el).serializeJSON(),
                $this = this;

                $.each(this.files , function(index, file) {
                    $this.fd.append('files['+index+']', file);
                });

                for(key in data){
                    if (data[key])
                        this.fd.append(key, data[key]);
                }

                $.ajax({
                    type: "POST",
                    url: "/api/news",
                    processData: false,
                    contentType: false,
                    data: $this.fd,
                    success: function (model) {
                        $this.collection.add(model);
                        $("#new-item-modal").modal('hide');
                    }
                });

                return false;
            },
            deletePhoto : function(e){
                var el = $(e.target).closest(".photo-item"),
                file_name = el.data("name"),
                photo_id = el.attr("id"),
                $this = this;
                
                if(file_name){
                    this.files = $.grep(this.files, function(opt){
                        opt.name != file_name;
                    })
                }

                if(!photo_id){
                    el.remove();
                }else{
                    $.ajax({
                        type: "DELETE",
                        url: "/api/news/" + $this.model.get("id") + "/photos/" + photo_id,
                        success: function (model) {
                            el.remove();
                        }
                    });
                }


            }
        });

        return ItemView;
    });
