define([
    'jquery',
    'underscore',
    'backbone',
    'views/list',
    'text!views/templates/contact_groups.html',
    'views/contact_form',
    'text!views/templates/contact.html',
    'text!views/templates/contact_group_user.html',
    'collections/contacts',
    'collections/contact-groups-users',
    'i18n!nls/fa-ir/labels',
], function($, _, Backbone, ListView, ListTpl, ContactFormView, ContactItemTpl, UserTpl, ContactsCollection, UsersCollection, Labels) {

    var ContactGroupsListView = ListView.extend({
        initialize: function(options) {
            this.events = _.extend({
                "click .itme-wrapper .title"            : "showGroupContacts",
                "click .add-contact"                    : "showAddContactPopup",
                "click .contact-item .btn-default"      : "showContactEditPopup",
                "click .contact-item .btn-danger"       : "deleteGroupContact",
                "click .show-group-list"                : "showGroupsList",
                "click .permission-users"               : "showUsersPermission",
                "click .modal .remove-user"             : "removeUser"
            }, this.events);
            this.listType = "groups";

            ContactGroupsListView.__super__.initialize.call(this, this.options);
        },
        render: function(fields) {
            var tpl = _.template(ListTpl);
            this.$el.html(tpl({
                labels: Labels,
                type: this.options.type,
                listType: this.listType || "groups",
                selectedGroup: this.selectedGroup ? this.selectedGroup.toJSON() : {}
            }));
            return this;
        },
        showGroupContacts: function(e) {
            var el = $(e.target).closest(".itme-wrapper"),
                id = el.attr("id"),
                $this = this;
            this.selectedGroup = this.collection.get(id);
            this.selectedGroup.users = new UsersCollection([]);
            this.selectedGroup.users.groupId = this.selectedGroup.get("id");
            this.listType = "contacts";
            this.render();
            $('#autocomplete-wrapper input').autocomplete({
                source: window.usersCollection.toJSON(),
                select: function(event, ui) {
                    $(this).val("");
                    if (!$this.selectedGroup.users.get(ui.item.id)) {
                        var model = new $this.selectedGroup.users.model();
                        model.save({
                            "group_id": $this.selectedGroup.get("id"),
                            "user_id": ui.item.id
                        }, {
                            success: function() {
                                model.set(ui.item);
                                var tpl = _.template(UserTpl);
                                $(".users-list", $this.$el).append(tpl(model.toJSON()));
                            }
                        });
                    }
                    return false;
                },
                minLength: 1,
                appendTo: '#autocomplete-wrapper',
            });
            this.contactsCollection = new ContactsCollection([]);
            this.contactsCollection.on("add change", this.renderContact, this);
            this.contactsCollection.groupId = this.selectedGroup.get("id");
            this.contactsCollection.fetch({
                success: function() {
                    $this.contactsCollection.each(function(model) {
                        $this.renderContact(model);
                    });
                }
            })
        },
        showUsersPermission: function() {
            var $this = this;
            $(".modal .users-list", this.$el).empty()
            this.selectedGroup.users.fetch({
                success: function(collection) {
                    collection.each(function(model) {
                        var tpl = _.template(UserTpl);
                        $(".users-list", $this.$el).append(tpl(model.toJSON()));
                    });
                    $(".users-list-popup", $this.$el).modal("show");
                }
            });
        },
        removeUser: function(e) {
            var el = $(e.target).closest(".list-group-item"),
                modelId = el.attr("id"),
                model = this.selectedGroup.users.get(modelId);
            model.destroy({
                success : function(){
                    el.remove();
                }
            });
        },
        showGroupsList: function() {
            delete this.selectedGroup;
            this.listType = "groups";
            this.render();
            this.renderItems()
        },
        showAddContactPopup: function() {
            this.popContactForm(new this.contactsCollection.model());
        },
        popContactForm: function(model) {
           
           this.contactFormView = new ContactFormView({
               model            : model,
               selectedGroup    : this.selectedGroup,
               collection       : this.contactsCollection
           });
            $("#new-item-modal", this.$el).html(this.contactFormView.$el);
            $("#new-item-modal", this.$el).modal({
                backdrop: "static"
            });
        },
       
        renderContact: function(model) {
            var tpl = _.template(ContactItemTpl),
                el = tpl(_.extend(model.toJSON(), {
                    labels: Labels
                }));
            if ($(".contacts-wrapper #" + model.get("id"), this.$el).length) {
                $(".contacts-wrapper #" + model.get("id"), this.$el).replaceWith(el);
            } else {
                $(".contacts-wrapper", this.$el).append(el);
            }
        },
        showContactEditPopup: function(e) {
            var itemId = $(e.target).parents('.contact-item').attr("id");
            this.itemModel = this.contactsCollection.get(itemId);
            this.popContactForm(this.itemModel);
        },
        
        deleteGroupContact: function(e) {
            var el = $(e.target).parents('.contact-item'),
                itemId = el.attr("id"),
                contact = this.contactsCollection.get(itemId);
            contact.destroy({
                success: function() {
                    el.remove();
                }
            })
        }

    });
    return ContactGroupsListView
});
