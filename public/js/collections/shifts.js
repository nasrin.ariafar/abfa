define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/collection/collection',
    'models/shift',
], function($, _, Backbone, App, BaseCollections, ItemModel) {
    var ShiftsCollection = BaseCollections.extend({
        model : ItemModel,

        url : function(){
            return '/api/shifts';
        }

    });
    return ShiftsCollection;
});
