define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/collection/collection',
    'models/region',
], function($, _, Backbone, App, BaseCollections, ItemModel) {
    var CausesCollection = BaseCollections.extend({
        model : ItemModel,

        url : function(){
            return '/api/regions';
        }

    });
    return CausesCollection;
});
