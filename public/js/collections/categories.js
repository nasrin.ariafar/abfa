define([
    'jquery',
    'underscore',
    'backbone', 
    'app',
    'base/collection/collection',
    'models/category',
    ], function($, _, Backbone, App, BaseCollections, ItemModel) {
        var CausesCollection = BaseCollections.extend({
            model : ItemModel,

            url : function(){
                return '/api/' + this.type.slice(0,-1) + '_categories';
            },
            parse : function(resp){
                var target = this.type.slice(0,-1),
                items = [];
                _.each(resp.items, function(item){
                    item.target = target;
                    items.push(item);
                });
                return items;
            }
        });
        return CausesCollection;
    });
