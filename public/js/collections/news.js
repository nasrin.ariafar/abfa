define([
    'jquery',
    'underscore',
    'backbone', 
    'app',
    'base/collection/collection',
    'models/news',
    'base/model/FilterModel'
    ], function($, _, Backbone, App, BaseCollections, ItemModel, FilterModel) {
        var NewsCollection = BaseCollections.extend({
            model : ItemModel,

            initialize: function(models, opt){
                this.opt = opt;
                this.filters = new FilterModel();
                this.autoPagination = true;
            },

            url : function(){
                return '/api/news';
            },
            parse: function( resp ){
                var items = [],
                $this = this;
                _.each(resp.items, function(item){
                    if(!item.deleted)
                        items.push(item);
                });
                return items;
            },
            nextPage : function(){
                var last = this.last();
                if(last){
                    if(this.filters.get("before") && this.filters.get("before") == last.get("creationDate"))
                        return
                    else{
                        this.filters.set("before", last.get("creationDate"));
                        this.fetch();
                    }
                }
            }

        });
        return NewsCollection;
    });
