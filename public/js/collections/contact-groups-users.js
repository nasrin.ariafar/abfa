define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/collection/collection',
    'models/contact-group-user',
], function($, _, Backbone, App, BaseCollections, ItemModel) {
    var ContactGroupsUsersCollection = BaseCollections.extend({
        model: ItemModel,
        url: function() {
            return '/api/contact-groups/' + this.groupId + "/users";
        },
        parse: function(resp) {
            var list = [],
                $this = this;

            _.each(resp.users, function(obj) {
                obj.group_id = $this.groupId;
                obj.user_id = obj.id;
                list.push(obj);
            });

            return list;
        }
    });
    return ContactGroupsUsersCollection;
});
