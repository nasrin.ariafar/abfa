define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'views/layout',
    'views/login'
    ], function($, _, Backbone, App, LayoutView, LoginView) {
        var AppRouter = Backbone.Router.extend({
            routes: {
                ''                  : 'homePage',
                'home'              : 'homePage',
                'changePassword'    : 'changePassword',
                'logout'            : 'logout'
            },

            before : function(){

                if(Backbone.history.fragment != "logout"){
                    if(window.currentUser.get("id"))
                        $("#main-container-wrapper").removeClass("before-login");
                    else
                        $("#main-container-wrapper").addClass("before-login");
                }
            },

            homePage : function() {
                if(window.currentUser.get("id")){
                    this.layoutView = new LayoutView();
                    this.layoutView.render();
                    this.layoutView.afterRender();
                }else
                    this.login();
            },

            login : function(){
                var loginView = new LoginView();
                $("#main-container-wrapper").html(loginView.render().$el);
            },
            changePassword : function(){
                this.layoutView = new LayoutView();
                //                this.layoutView.render();
                this.layoutView.changePassword();
            },
            logout : function(){
                $.ajax({
                    url     : '/user/logout',
                    type: "POST",
                    success : function(resp){
                       window.location = "";
                    },
                    error : function(resp){
                    }
                }, this);
            }
            
        });
    
        return AppRouter;
    });
