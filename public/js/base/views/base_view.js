define([
    'jquery',
    'backbone',
    'underscore',
    'jquery-helper'
    ], function($, Backbone, _, jqHelper) {
        
        var BaseView = function(options) {
            this.bindings = [];
            Backbone.View.apply(this, [options]);
        };
        _.extend(BaseView.prototype, Backbone.View.prototype, {

            bindTo: function(model, ev, callback) {

                model.bind(ev, callback, this);
                this.bindings.push({
                    model: model,
                    ev: ev,
                    callback: callback
                });
            },

            unbindFromAll: function() {
                _.each(this.bindings, function(binding) {
                    binding.model.unbind(binding.ev, binding.callback);
                });
                this.bindings = [];
            },

            dispose: function() {
                this.unbindFromAll();
                this.unbind();
                this.remove();
            }
        });

        _.mixin({
            percentage : function(per){
                if(localStorage.getItem('locale') != 'en-us'){
                    return '%' + __(per);
                }

                return __(per) + '%';
            },
            fuzzyNum: function(number){            
                var KILO    = 1000,
                    MEGA    = 1000000,
                    GIGA    = 1000000000,
                    number  = parseInt(number);
            
                 if(Math.abs(number) < KILO)
                        return number.toFixed(3);
                 if (Math.abs(number) < MEGA)
                        return (number/KILO).toFixed(3) + ' K';
                 if (Math.abs(number) < GIGA)
                     return (number/MEGA).toFixed(3) + ' M';
                 return (number/GIGA).toFixed(3) + ' B';
            },     
            
            changeSetColor : function(num){              
                if(parseFloat(num) >= 0)
                    return 'ch-positive';
                else
                    return 'ch-negative';
            },

            gDateFormat : function(date){
                return date.getFullYear() + '-' + (date.getMonth() + 1) + - + date.getDate();
            },

            stock_status : function(type){
                switch(type){
                    case 'A':
                        return 'status_A';                        
                        break;
                    case 'I':
                        return 'status_I';                        
                        break;
                    case 'AG':
                        return 'status_AG';                        
                        break;
                    case 'AS':
                        return 'status_AS';                        
                        break;
                    case 'AR':
                        return 'status_AR';                                           
                        break;
                    case 'IG':
                        return 'status_IG';                        
                        break;
                    case 'IS':
                        return 'status_IS';          
                        break;
                    case 'IR':
                        return 'status_IR';                              
                        break;
                }
            },

            ucFirst : function(str) {

                if (typeof str != "string") {
                    return str;
                }
                return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
            },

            getPersiandate : function(gDate){
                var t = new Date(gDate.slice(0,10)).getTime();
                return new persianDate(t).format("YYYY/MM/DD");

            }

        })

        BaseView.extend = Backbone.View.extend;
        return BaseView;
    });
