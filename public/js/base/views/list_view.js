define([
    'jquery',
    'jquery-ui',
    'backbone',
    'underscore',
    'base/views/base_view',
    'i18n!nls/labels',
    ], function($, jqueryui, Backbone, _, BaseView, Labels) {
        var BaseListView = BaseView.extend({
            events: {

            },
                
            initialize: function() {
                _.bindAll(this, 'render', 'renderItem', 'updatePaging');
                var self = this;
            
                this.items = [];
                if (!this.loaded && !this.collection._notFetch) {
                    this.collection.fetch({
                        add     : true,
                        silent  : true,
                        update  : true
                    });
                }else{
                    var count = (this.collection.filters.count) ? this.collection.filters.count : -1;
                
                    if(this.collection.filters.get('offset') + parseInt(this.collection.filters.get('limit')) <=  parseInt(count)){
                        $('.hold-on-loading').data('loading', false);
                        this.resetPagination();
                    }
                }

                this.collection.bind('add', function(model, collection, opt) {
                    self.renderItem(model, undefined, true);
                    if (opt && opt.callback) {
                        opt.callback.call(self);
                    }
                });

                this.collection.on('fetchSuccess', function(resp, collection) {
                    this.renderItems();
                }, this);
            },
        
            render: function() {
                return this;
            },
        
            renderItem: function() {},
        
            renderItems: function(start) {
                start = start != undefined ? start : this.collection.filters.get('offset');
                for (var i = start; i < this.collection.models.length; i++) {
                    this.renderItem(this.collection.models[i], i);
                }
                this.afterRenderItems();
                this.updatePaging();
            },
        
            disposeAllItems : function(){
                for(var index in this.items){
                    this.items[index].remove();
                };
            },
        
            afterRenderItems: function() {},
        
            loadMore: function() {
                this.collection.increase().fetch({
                    add     : true,
                    silent  : true,
                    update  : true
                });
            },
        
            resetPagination : function(){
                var $this = this;
            
                $('.hold-on-loading').waypoint(function(event, direction) {
                    if (direction == 'down' && !$('.hold-on-loading').data('loading')) {
                        $this.loadMore();
                        $('.hold-on-loading').data('loading', true);
                        $('.hold-on-loading').css('display', 'block');
                    }
                }, {
                    offset: function() {
                        return $.waypoints('viewportHeight') - $(this).outerHeight();
                    },
                    triggerOnce: false,
                    onlyOnScroll: true
                });
            },
                
            updatePaging: function() {
                if(isNaN(this.collection.filters.count))
                    return;
            
                if (this.collection.filters.get('offset') + this.collection.filters.get('limit') > this.collection.filters.count) {
                    $('.hold-on-loading span').html(Labels['no_more_items']);
                    $('.hold-on-loading').css('display', 'none');
                } else {
                    $('.hold-on-loading span').html(Labels['loading_more']);
                    $('.hold-on-loading').css('display', 'block');
                }
                if (this.collection.models.length < 1) {
                    $('.hold-on-loading').css('display', 'none');
                    $('.hold-on-loading span').html(Labels['no_items']);
                }
            }
        });
    
        _.mixin({
            datePicker : function(timestamp){
                var d = new Date(timestamp);
                return d.getFullYear() + '/' + d.getMonth() + '/' + d.getDate();
            },
            documentItem: function(document) {
                var location = "";
                _.each(document, function(doc) {
                    if (doc.order == 1)
                        location = doc.location;
                });

                if(location[0] == '/') return location;
                return '/' + location;
            }
        });

        return BaseListView;
    
   
    });




