define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            defaults: {
                'id'            : null,
                'title'         : null,
                'firstname'     : null,
                'lastname'      : null,
                'cellPhone'     : null,
                'homePhone'     : null,
                'image'         : null
            },

            url : function(){
                 return '/api/contact-groups/' + this.get("group_id") + "/contacts" + ( (this.get("id")) ? '/' + this.get("id") : '');
            },
            
            saveImage : function(fd, callback){
                var $this = this;
                $.ajax({
                    async : false,
                    type: "POST",
                    url: $this.url() + '/photo',
                    processData: false,
                    contentType: false,
                    data: fd,
                    success: function (obj) {
                        callback && callback();
                    },
                    error : function(error){
                        
                    }
                });
            }
        });
        return ItemModel;
    });


