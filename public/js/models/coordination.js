define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            defaults: {
                'id'            : null,
                'title'         : null,
            },

            url : function(){
                return "api/coordinations" + ( (this.get("id")) ? '/' + this.get("id") : '');
            }
        });
        return ItemModel;
    });


