define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            defaults: {
                'id'            : null,
                'title'         : null,
                'headline'      : null,
                'body'          : null,
                'tags'          : []
            },

            url : function(){
                return "api/external-news" + ( (this.get("id")) ? '/' + this.get("id") : '');
            },

            publishAction : function(action, callback){
                var $this = this;
                $.ajax({
                    async : false,
                    type: "POST",
                    url: $this.url() + '/' + action,
                    success: function (obj) {
                        callback && callback();
                    },
                    error : function(error){

                    }
                });
            }
        });
        return ItemModel;
    });


