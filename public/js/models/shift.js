define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({

            url : function(){
                 return '/api/shifts' + ( (this.get("id")) ? '/' + this.get("id") : '');
            }
        });
        return ItemModel;
    });


