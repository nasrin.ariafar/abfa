define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            defaults: {
                'id'            : null,
                'email'         : null,
                'firstname'     : null,
                'lastname'      : null,
                'displayName'   : null,
                'state'         : null,
                'id'            : null,
                'username'      : null,
                'photo'         : null,
                'region'        : null,
                'levels'        : null,
                'position'      : null,
                'levels'        : [],
                'region'        : null,
                'registrationCode' : null,
                'accessibility'    : null
            },

            url : function(){
                return "api/users" + ( (this.get("id")) ? '/' + this.get("id") : '');
            },

//            parse : function(obj){
//                if(obj.type != "ADMIN")
//                    return obj;
//
//                else
//                    return;
//            }
        });
        return ItemModel;
    });

