({
    appDir: './',
    baseUrl: "./js",
    mainConfigFile: './js/main.js',
    dir: './release',
    logLevel: 0,
    preserveLicenseComments: !1,
    optimize: "uglify2",
    removeCombined: true,
    findNestedDependencies: true,
    optimizeCss: 'standard',
    waitSeconds: 60,
    wrapShim: true,
    skipDirOptimize: true,
    keepBuildDir: false,
    // 3rd party script alias names
    paths: {
        'jquery': 'vendor/jquery/dist/jquery',
        'jquery-ui': 'vendor/jquery-ui/js/jquery-ui-1.10.4.custom',
        'underscore': 'vendor/underscore-amd/underscore',
        'underscore_string': 'vendor/underscore.string/lib/underscore.string',
        'backbone': 'vendor/backbone-amd/backbone',
        'bootstrap': 'vendor/bootstrap/dist/js/bootstrap',
        'text': 'vendor/requirejs-text/text',
        'backbone.routefilter': 'vendor/backbone.routefilter/src/backbone.routefilter',
        'json': 'vendor/requirejs-json/json',
        'i18n': 'vendor/requirejs-i18n/i18n',
        'waypoints': 'lib/waypoints',
        'jquery-helper': 'lib/jquery-helper',
        'bootstrap-select': 'vendor/silviomoreto-bootstrap-select/bootstrap-select.min',
//        'pwt-date': 'vendor/pwt.datepicker-master/js/pwt-date',
//        'pwt-datepicker': 'vendor/pwt.datepicker-master/js/pwt-datepicker',
        'tagit': 'vendor/jQuery-Tagit-master/js/tagit',
        'moment': 'vendor/moment/min/moment-with-langs.min',
//        'moment-locale' : 'vendor/moment/min/moment-with-locales.min',
        'moment-timezone': 'vendor/moment-timezone/builds/moment-timezone-with-data.min',
//        'fullcalendar': 'vendor/fullcalendar-2.2.0/fullcalendar',
        'persianDate': 'vendor/pwt.datepicker-master/lib/persian-date',
//        'moment-jalaali' : 'vendor/moment-jalaali-master/build/moment-jalaali'
    },
    // Sets the use.js configuration for your application
    use: {
        'underscore.string': {
            deps: ['underscore']
        },
        'backbone': {
            deps: ['underscore', 'jquery']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'backbone.routefilter': {
            deps: ['backbone', 'underscore']
        },
        'waypoints': {
            deps: ['jquery']
        },
        'jquery-helper': {
            deps: ['jquery']
        },
        'pwt-date': {
            deps: ['jquery']
        },
        'persianDate': {
            deps: ['jquery']
        },
        'plupload.queue': {
            deps: ['plupload']
        },
        'jquery-ui': {
            deps: ['jquery']
        },
        'tagit': {
            deps: ['jquery', 'jquery-ui']
        },
        'moment-timezone': {
            deps: ['moment']
        },
        'moment-jalaali': {
            deps: ['moment-timezone']
        },
    }, // end Use.js Configuration
    // Modules to be optimized:


    name: "main"


})
