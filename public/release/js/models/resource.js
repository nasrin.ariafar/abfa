define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            defaults: {
                'id'            : null,
                'title'         : null,
                'unit'          : null,
                'category'      : null
            },

            url : function(){
                return "api/resources" + ( (this.get("id")) ? '/' + this.get("id") : '');
            }
        });
        return ItemModel;
    });


